---
title: Using a Wiki for Collaborative Documentation TOSW - Introduction
permalink: wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Introduction/
layout: wiki
tags:
 - Using a Wiki for Collaborative Documentation TOSW
---

The purpose of this guide is to teach you how to use a MediaWiki
instance to write documentation by a community. It follows the practices
of [The Open Source Way](/wiki/The_Open_Source_Way "wikilink").

***FIXME: ADD MORE CONTENT***
