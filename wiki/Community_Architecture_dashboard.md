---
title: Community Architecture dashboard
permalink: wiki/Community_Architecture_dashboard/
layout: wiki
tags:
 - The Open Source Way book
---

Red Hat's Community Architecture team supports our executive leadership
team by providing a dashboard-view of community health information. The
communities on the dashboard list vary depending on interest and need.
For example, we are not actively watching the health of our Linux kernel
or GCC contributions. This is because those communities are
self-sustaining and extremely healthy. Instead, the dashboard watches
important, struggling, and nascent community efforts that either the
team or the corporate leadership team are interested in tracking.

Current aspects of the dashboard:

-   Data gathered through [EKG](https://fedorahosted.org/ekg)

