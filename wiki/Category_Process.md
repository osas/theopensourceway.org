---
title: Category:Process
permalink: wiki/Category:Process/
layout: tagpage
tag: Process
---

This category contains processes that help a project or organization
measure, attain, and sustain following the open source way.
