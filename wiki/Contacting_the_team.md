---
title: Contacting the team
permalink: wiki/Contacting_the_team/
layout: wiki
---

Whoever we are, here are the places to find us for:

-   Asking questions;
-   Learning how to contribute;
-   Debating, in a nice way;
-   ... and so forth.

Email list
----------

<https://fedorahosted.org/mailman/listinfo/tosw>

Chat (IRC)
----------

-   Webchat - <http://webchat.freenode.net/?channels=opensource.com>
-   IRC client - \#opensource.com on irc.freenode.net

