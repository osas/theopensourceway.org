---
title: Marketing the open source way
permalink: wiki/Marketing_the_open_source_way/
layout: wiki
tags:
 - The Open Source Way book
---

*This chapter discusses how the domain of marketing is handled in the
open source way, with more detail than the principles previously
discussed.*

Relevant principles
-------------------

-   [How\_to\_loosely\_organize\_a\_community\#Start\_open\_marketing\_soonest](/wiki/How_to_loosely_organize_a_community#Start_open_marketing_soonest "wikilink")
    (http://bit.ly/TOSWOpenMktg1st)
-   [Introduction\#Open
    marketing](/wiki/Introduction#Open_marketing "wikilink")
    (http://bit.ly/TOSWOpenMktg)

### Supporting outside material

-   [Unsecret Source: marketing strategy the open source
    way](http://www.johnderry.com.au/76/unsecret-source-marketing-strategy-open-source-way)

Loose Ideas
-----------

-   Product Marketing vs. Strategic Marketing - we need to break these
    up. Strategy is JUST as important as Product marketing.
-   What does Fedora mktg do?
-   Other community examples (need links):
    -   OpenOffice
    -   OpenSUSE
    -   wikimedia / wikipedia
-   Virt Group is excellent example of a group more or less doing
    self-marketing - through magazine articles, excellent and
    informative blog posts, speaking engagements

Marketing Life Cycle
--------------------

-   The life cycle defines a way to consistently:
    -   Define goals
    -   Identify gaps
    -   Figure out what needs to be done to fill gaps to help reach
        goals (these three are the strategic part)
-   Making the Product / Completing the Idea - is the actual Product
    Life Cycle (in Fedora - this is essentially the entire 6-month
    period - from start to release)
    -   When !idea / gap gets completed or is close - move on to the
        Product Marketing Phase
-   At some point - Life Cycle is closed, just like a Product Life
    cycle. At this point - it's time to assess, see where you landed,
    and start over fresh. This means:
    -   Revalidating goals
    -   Rebooting Marketing life cycle

Important Strategy Bits to consider
-----------------------------------

-   Getting people to move from thinking about a short-term product
    cycle to a long-term strategy may be difficult.
-   We should think BEYOND the 6-month (in Fedora, other lengths in
    other places) cycle - beyond the "Point A to Point B" - or
    basically, beyond the "start Making Product, finish Making Product"
    length of time.
    -   Point A and Point B should be MILESTONES on the way to bigger
        achievements.
    -   Should think about: Where does this community want to be 1 year
        from now? 3 years from now?
-   Getting buy-in to strategy is IMPORTANT.
    -   This means: Letting community members participate in MANY parts
        of strategy development. If you want community members to
        contribute to strategy execution, they need to believe in it,
        and they need to believe that they were part of developing the
        strategy.
    -   Pieces like Research - coming up with DIFFERENT strategies, and
        seeing how those scenarios might work out - discussion of what
        people want to work on - are all pieces that should be written
        down and bought into beforehand.
-   Talking about WHY strategy is important is something that should be
    done before ANYTHING ELSE.
    -   How can strategy help us?
    -   How has strategy helped other people? - Demonstrating what
        others have done
    -   Why do we need strategy? - If people don't see and buy into the
        WHY, they will never buy into the HOW.

Product Marketing
-----------------

-   Invite community members to hawk their own stuff. This is
    fundamentally different from a closed-marketing environment. *If
    people are excited about their product and want to market it, you
    should be excited too.*
-   Encourage community members to see out opportunities such as:
    -   Magazine articles - market within their own domain
    -   Blog, blog, blog
    -   Microblog, microblog, microblog (aka: twitter, identi.ca)
    -   Social Community interactions - Talk at LUGs. Form a FB group.
        Form a linkedin group. Talk at conferences.

