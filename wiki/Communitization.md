---
title: Communitization
permalink: wiki/Communitization/
layout: wiki
tags:
 - Glossary
---

This a stub page that explains the what, how, and why to apply a
community process to an idea. The idea might be for a product, a new
social awareness group, or a gardening community.

Definition
----------

*Communitization* is the process of applying open collaborative
community principles to an idea or an existing project or product.
Communitization can be thought of as opening things up to allow for
external contributions.

A *project* is the collective effort by a group of people to create some
output. It is generally an open source community project, but it may be
a new project not yet decided if it is going to be open, or acquired
code or content and/or a team who began work as a non-open project.

A *product* is the effort of taking the output (such as source code and
content) from a project and making it so a customer can pay a vendor for
the added-value, such as service and support.

Products have communities, just as projects do. There are points if
similarity and difference between these community types.

Product communities
-------------------

**If your product is open source, it needs a project community.**

-   Attract people passionate about using and evangelizing the product.
-   Draw people looking for how-to answers.
-   Is the base that majority of contributors come from. (E.g. [75% in
    Fedora](http://www.cyber-anthro.com/beta-an-exploration-of-fedora%E2%80%99s-online-open-source-development-community/).)

Project communities
-------------------

**If you are open source, you need a project community.**

Your product may not matter, but if you want future contributors you
need something people can use and care about.

-   Attract people passionate about participating in the lifecycle of
    the project.
    -   They want to give back, especially in
        [Communities\_of\_practice\#Invite\_different\_levels\_of\_participation
        legitimate peripheral
        activities](/wiki/Communities_of_practice#Invite_different_levels_of_participation_legitimate_peripheral_activities "wikilink").
    -   End results may have less meaning than process.
-   Draw people who know the how-to answers and want to learn how-to do
    more, or provide more functionality.

