---
title: Legal
permalink: wiki/Legal/
layout: wiki
tags:
 - Legal
---

This page gathers together all of pages with content related to the
legal aspects of this website and book.

-   [Contribution policy](/wiki/Contribution_policy "wikilink") is a simple
    document you agree to when you obtain an account to services related
    to theopensourceway.org.
-   [The Open Source
    Way:Copyrights](/wiki/The_Open_Source_Way:Copyrights "wikilink") specifies
    the copyrights for the materials in theopensourceway.org.
-   [The Open Source Way:Privacy
    policy](/wiki/The_Open_Source_Way:Privacy_policy "wikilink") contains or
    points to relevant privacy information for contributors with
    accounts on theopensourceway.org.
-   [The Open Source Way:General
    disclaimer](/wiki/The_Open_Source_Way:General_disclaimer "wikilink")
    attempts to make clear what is what, whose is whose, and so forth.

