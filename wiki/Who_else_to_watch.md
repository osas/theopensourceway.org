---
title: Who else to watch
permalink: wiki/Who_else_to_watch/
layout: wiki
tags:
 - The Open Source Way book
---

This section generally lists individuals and organizations external to
Red Hat that are useful to watch their approach to community.

A sustainable community underpinning is continuous improvement. To
improve, we learn from each other.

For that reason, this book needs *your* help. Add to these lists.

We could list names and organizations endlessly.

People
------

-   Brian King, Mozilla Community Manager, Europe
-   Dawn Foster, PuppetLabs Community Manager
-   Jono Bacon, Senior Director of Community at XPRIZE Foundation &
    Author of Art of Community
-   Mary Colvig, Mozilla Community Engagement
-   Pierros Papadeas, Mozilla Community Architect

Groups
------

-   [The FLOW(Free/Libre/Open Works)
    Syllabus](http://osi.xwiki.com/bin/Projects/flow-syllabus)
-   [School of Open](http://SchoolOfOpen.p2pu.org/)
-   [Teaching Open Source](http://TeachingOpenSource.org)

