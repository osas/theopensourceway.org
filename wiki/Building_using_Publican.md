---
title: Building using Publican
permalink: wiki/Building_using_Publican/
layout: wiki
tags:
 - How to
 - Contributing to TOSW
---

Publican is available for [Linux distributions and Microsoft
Windows](http://jfearn.fedorapeople.org/Publican/chap-Users_Guide-Installing_Publican.html).
More information can be found here:

<https://fedorahosted.org/publican>

The [Publican User Guide](http://jfearn.fedorapeople.org/Publican/) is
linked from that page.

To install Publican in Fedora, use the graphical installer at *System
&gt; Administration &gt; Add/Remove Software* or on the command line use
`yum install publican publican-fedora`. The second item is the package
for building with the Fedora brand.

The code in the repository may be set to use a different brand package,
so the instructions include calling out the Fedora brand using the
included `fedora.cfg`.

1.  Check out the source as explained in [Using git
    repository](/wiki/Using_git_repository "wikilink").
2.  Make sure Publican and a useful brand package are installed:
        rpm -qa | grep publican
        su -c 'yum install publican publican-fedora'

3.  Change in to the directory with the source and run the Publican
    command to build using a specific brand:
        cd src/tosw/
        publican build --formats html,pdf,epub --langs en-US --config fedora.cfg

If you have any problems building, ask on the [mailing
list](https://lists.fedorahosted.org/mailman/listinfo/tosw).
