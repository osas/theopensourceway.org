---
title: OSS CWG Legal Referrals
permalink: wiki/OSS_CWG_Legal_Referrals/
layout: wiki
tags:
 -  Open source consulting working group
---

Initial contributions to this list of legal firms/lawyers familiar with
open source licenses and practices was contributed by community member
[Black Duck Software](http://www.blackducksoftware.com). Additions to
this list are welcome, and an eventual end goal is to make this list
rate-able by community participants.

<hr width="100%" size="2" />
<h1>
<strong>Argentina</strong>

</h1>
<strong>Diego Nebot  
Gómez y Asociados  
</strong>H. Yrigoyen 10 2º 8º Godoy Cruz  
Mendoza 5500  
Tel: 54 261 154852483   
Email: <dnebot@gmail.com>

<hr width="100%" size="2" />
<h1>
<strong>Belgium</strong>

</h1>
<strong>Bastiaan Bruyndonckx, Counsel  
Linklaters  
</strong>Brederodestraat 13  
Brussels, 1000  
Belgium  
Tel: +32-2 501 91 96  
Email: <bastiaan.bruyndonckx@linklaters.com>

<hr width="100%" size="2" />
<h1>
<strong>Brazil</strong>

</h1>
<strong>Vitor Cruz, Partner  
Chediak Advogados</strong>  
Av. Presidente Juscelino Kubitschek 1726, 18 floor  
Sao Paulo, 04543-000  
Brazil  
Email: <vitor.cruz@clcmra.com.br>

<strong>Rafael Gomes, Partner  
Chediak Advogados</strong>  
Av Juscelino Kubitschek 1726, 18 floor  
Sao Paulo, 04543-000  
Brazil  
Email: <rafael.gomes@clcmra.com.br>

<strong>Deborah Valcazara, Ms  
Chediak Advogados</strong>  
Av. Presidente Juscelino Kubitschek 1726, 18 floor  
Sao Paulo, 04543-000  
Brazil  
Email: <deborah.valcazara@clcmra.com.br>

<strong>Renato Blum, CEO  
Opice Blum Attorneys at Law</strong>  
Al Joaquim Eugenio de Lima, 680 - 1andar  
Sao Paulo, 01403-000  
Brazil  
Email: <renato@opiceblum.com.br>

<hr width="100%" size="2" />
<h1>
<strong>Canada</strong>

</h1>
<strong>Bernie Eischen, Lawyer  
Bernadette Eischen Professional Corporation  
</strong>21 Hyde Park Way  
Ottawa, ON K2G 5R7  
Email: <beischen@sympatico.ca>

<strong>Christine Ing, Consultant  
Blake, Cassels & Graydon LLP  
</strong>199 Bay Street  
Suite 4000  
Toronto, ON M5L 1A9  
Tel: (416) 863-2667  
Email: <cgi@blakes.com>

<strong>Mark McCans, Associate  
Blake, Cassels & Graydon LLP  
</strong>199 Bay Street  
Suite 4000  
Toronto, ON M5L 1A9  
Tel: (416) 863-2710  
Email: <mark.mccans@blakes.com>

<strong>Tom Reaume, Lawyer  
Fraser Milner Casgrain  
</strong>1420-99 Bank Street  
Ottawa, ON K1P 1H4  
Tel: (613) 783-9610  
Email: <tom.reaume@fmc-law.com>

<strong>Paul Armitage, Partner  
McCarthy Tetrault LLP  
</strong>777 Dunsmuir Street  
Suite 1300  
Vancouver, BC V7Y 1K2  
Tel: (604) 643-5895   
Email: <parmitage@mccarthy.ca>

<strong>Bob Nakano, Partner  
McCarthy Tetrault  
</strong>66 Wellington St. West  
Toronto, ON M5K1E6  
Tel: (416) 601-7852  
Email: <bnakano@mccarthy.ca>

<strong>Darryl Bilodeau, Lawyer  
Morisawa De Koven PC  
</strong>c/o Innova Law Group, 220-1140 Morrison Drive  
Ottawa, ON K2H6Y6  
Email: <darryl@businesslawadvice.com>

<strong>Robert Percival, Partner  
Norton Rose Canada LLP  
</strong>Royal Bank Plaza, South Tower  
Suite 3800, 200 Bay Street  
Toronto, ON M5J 2Z4  
Tel: (416) 216-4075  
Email: <robert.percival@nortonrose.com>

<strong>Roger Watkiss, Partner  
Norton Rose Canada LLP  
</strong>2300-79 Wellington St. W.  
Toronto, ON M5k 1H1  
Tel: (416) 202-6716  
Email: <roger.watkiss@nortonrose.com>

<hr width="100%" size="2" />
<h1>
China

</h1>
<strong>Philip Qu </strong>  
<strong>TransAsia Lawyers</strong>  
Suite 2218 China World Office 1  
1 Jianguomenwai Avenue  
Bejijing 100004, China  
Tel: +86 10 6505 8188  
Fax +86 10 6505 8189/98  
Email: <pqu@transasialawyers.com>

<strong>Lishan Wang, Legal Manager  
Hanwang Technology Co. Ltd </strong>  
Building No. 5, Zhongguancun Software  
Bejijing 100093, China  
Tel: (368) 335-7565  
Email: <wanglishan@hanwang.com.cn>

<hr width="100%" size="2" />
<h1>
Denmark

</h1>
<strong>Frederik Ploug Sarp, Attorney </strong><strong>  
Kromann Reumert </strong>  
Sundkrogsgade 5  
Copenhagen, Copenhagen 2100  
Denmark  
Tel: +45 70 12 12 11  
Email:<span style="text-decoration: underline  [mailto:Fps@kromannreumert.com">Fps@kromannreumert.com\]</span>

<hr width="100%" size="2" />
<h1>
Finland

</h1>
<strong>Mikko-Pekka Partanen, Associate</strong><strong>  
Bird & Bird Attorneys Ltd.</strong>  
Mannerheimintie 8  
00100 Helsinki, Finland  
Tel: +358 9 622 6670  
Email: <mikko-pekka.partanen@twobirds.com>

<hr width="100%" size="2" />
<h1>
France

</h1>
<strong>Benedicte Deleporte, Attorney - Partner</strong><strong>
</strong><strong>  
Deleporte Wentz Avocat </strong>  
7, rue de Madrid - 75008 Paris  
France  
Tel: +33 (0)1 44 90 17 10  
Email: <bdeleporte@dwavocat.com>

<strong>Vincent Pollard, Advocate</strong><strong>  
TAJ Member of Deloitte Touche</strong>  
10, Place de la Joliette BP62544  
Les Docks - Atrium 10.5  
Marseille Cedex 02, 13567  
France  
Tel: +33 (0)4 91 59 84 75  
Email:<vpollard@taj.fr>

<strong>Jean-marc Puech, Attorney at Law  
Cabinet Jmplaw </strong>  
59, Boulevard Exelmans  
Paris, France 75016  
Email:<jmpuech@jmplaw.eu>

<hr width="100%" size="2" />
<h1>
<strong>Germany</strong>

</h1>
<strong>Ciaran Farrell, Legal Advisor </strong><strong>  
Novell, Deutschlandt</strong>  
Maxfeldstrasse 5  
Nuremberg, 90409  
Germany  
Email: <cfarrell@novell.com>

<strong>Dr. jur. Fabian Schafer  
Bird & Bird LLP</strong>  
Taunusaniage 1  
Frankfurt/Main, 60329  
Germany  

Email: <fabian.schaefer@twobirds.com>

<strong>Dr. Christopher M. De Nicolo</strong>  
Rechtsanwalt  
Prinz-Ludwig-Strabe 15  
93055 Regensburg  
Tel: +49 941 78531670  
Email: <kanzlei@denicolo.de>

<strong>Oliver Block, Patent Attorney  
Patentanwaltskanzlei</strong>  
Rathauspromenade 6  
Berlin, 13437  
Germany  
Email: <info@block-patent.de>

<hr width="100%" size="2" />
<h1>
India

</h1>
<strong>Karthik Kannappan, Assistant Manager – Global Contracts and
Legal Compliance  
Persistent Systems Limited </strong>  
402, "Bhageerath"  
Senapati Bapat Road  
Pune, India, 411016  
Tel: (965) 772-6849  
Email: <karthik_kannappan@persistent.co.in>

<strong>Raghunath Ananthapur, Lawyer  
Tatva Legal</strong>  
2580, 8 B Main Road  
Banashankari II Stage  
Bangalore, India, 560070  
Tel: (990) 019-2977  
Email: <raghunath.ananthapur@tatvalegal.com>

<strong>Shivacharan Reddy  
Tatva Legal</strong>  
Tel: (993) 522-8280  
Email:
[shiva.charan@tatvalegal.com](mailto:raghunath.ananthapur@tatvalegal.com)

<hr width="100%" size="2" />
<h1>
Ireland

</h1>
<strong>Aoife Sexton, Head of Technology and Telecoms  
Maples and Calder, Dublin  
</strong>75 St. Stephens Green  
Dublin 2, Ireland  
Tel: +353872443278  
Email: <aoife.sexton@maplesandcalder.com>

<hr width="100%" size="2" />
<h1>
Italy

</h1>
<strong>Thomas Contin, Lawyer  
Studio Legale Contin  
</strong>Via Fontana, 22  
Milano 20131  
Italy  
Tel: +393337906976  
Email: <thomas.contin@contin-law.com>

<hr width="100%" size="2" />
 

<h1>
Japan

</h1>
<strong>Hiroshi Kamiyama, Attorney at law  
Hibiya Park Law Offices  
</strong>Hibiya Marine Bldg.,  
5th Floor 5-1 Yurakucho 1-chome, Chiyoda-ku  
Tokyo, Japan, 100-0006  
Tel: 81-3-5532-8888  
Email: <kamiyama@hibiyapark.net>

<strong>Mr. Hisamichi Okamura, Attorney at law  
Eichi Law Offices, LLC</strong>  
Toranomon 40 MT Building 9F  
13-1, Toranomon 5-chome, Minatoku  
Tokyo, Japan, 105-0001  
Tel: 81-3-5425-2561  
Email: <okamura@mail.law.co.jp>

<hr width="100%" size="2" />
<h1>
Luxembourg

</h1>
<strong>Erwin Sotiri, Managing Partner  
Jurisconsul</strong>  
70 Grand rue  
Luxembourg, 1660  
Luxembourg  
Tel: +352 26389808

<hr width="100%" size="2" />
<h1>
Sweden

</h1>
<strong>Claudia Wallman, Specialist Counsel  
Mannheimer Swartling</strong>  
Norrlandsgatan 21  
Stockholm, 11187  
Sweden  
Tel: +46709777356  
Email: <clw@msa.se>

<strong>Jens Kinnander, Partner, Advokat  
Glimstedt</strong>  
Byggmästaregatan 5  
Lund, SE-22105  
Sweden  
Tel: +46 (0)709 991020  
Email: <jens.kinnander@glimstedt.se>

<hr width="100%" size="2" />
<h1>
United Kingdom

</h1>
<strong>Christian Bartsch, Partner  
Bird & Bird LLP  
</strong>15 Fetter Lane  
London, UT EC4A 1JP  
United Kingdom  
Email: <Christian.bartsch@twobirds.com>

<strong>Julia Bishop  
Bird & Bird LLP</strong>  
15 Fetter Lane  
London, UT EC4A 1JP  
United Kingdom  
Email: <julia.bishop@twobirds.com>

<strong>Ian Edwards, Solicitor  
Bird & Bird LLP</strong>  
15 Fetter Lane  
London, UT EC4A 1JP  
United Kingdom  
Email: <ian.edwards@twobirds.com>

<strong>Robert Lyne, Associate  
Bird & Bird LLP</strong>  
15 Fetter Lane  
London, UT EC4A 1JP  
United Kingdom  
Email: <robert.lyne@twobirds.com>

<strong> Richard Grahman, Partner  
Edwards Angell Palmer & Dodge UK LLP </strong>  
Dashwood  
69 Old Broad Street  
London, EC2M 1QS  
United Kingdom  
Email:<rgraham@eapdlaw.com>

<strong>James Wilkinson, Associate  
Kemp Little LLP</strong>  
Cheapside House  
138 Cheapside  
London, EC2V 6BJ  
United Kingdom  
Email: <james.wilkinson@kemplittle.com>

<strong>Mr. Andrew Katz  
Moorcrofts</strong>  
Mere House Mere Park  
Dedmere Road, SL7 1FJ  
Marlow, Buckinghamshire  
United Kingdom  
Email: <andrew.katz@moorcrofts.com>

<strong>Katie Hill, Solicitor  
Moorcrofts</strong>  
James House Mere Park  
Dedmere Road, SL7 1FJ  
Marlow, Buckinghamshire  
United Kingdom  
Email: <katie.hill@moorcrofts.com>

<strong> Peter Lee, Associate  
<strong>Bird & Bird LLP</strong>  
15 Fetter Lane  
London, UT EC4A 1JP  
United Kingdom  
Email:<peter.lee@twobirds.com>

<strong Brian Meenagh, Associate<br /> <strong>Latham & Watkins
LLP</strong>  
99 Bishopsgate  
London, UK EC2M 3XF  
Email:<brian.meenagh@lw.com>

<hr width="100%" size="2" />
<h1>
<strong>California</strong>

</h1>
<strong>Jonathan Chen, Associate Attorney  
Cooley, Godward, Kronish LLP  
</strong>Five Palo Alto Square  
3000 El Camino Real  
Palo Alto, CA 94306  
USA  
Tel: +1 650 843-5281  
Email: <jchen@cooley.com>

<strong>Richard Gerould, Attorney  
Gerould & Player, LLP  
</strong>1295 Dana Avenue     
Palo Alto, CA 94301  
USA  
Email: [Rich@gerouldplayer.com](mailto:rich@gerouldplayer.com)

<strong>Andrew Hall, IP Group  
Fenwick & West LLP  
</strong>555 California Street, 12th Floor    
San Francisco, CA 94104  
USA  
Email: <ahall@fenwick.com>

<strong>Amy Hansen, Associate  
Goodwin Procter  
</strong>135 Commonwealth Ave.    
Menlo Park, CA 94025  
USA  
Email: <ahansen@goodwinprocter.com>

<strong>Kevin Lam, Attorney  
Goodwin Procter LLP  
</strong>135 Commonwealth Dr.  
Menlo Park, CA 94025  
USA  
Tel: 1+ (650) 752-3210  
Email:<Klam@goodwinprocter.com>

<strong>Joseph Morris, Partner  
mod4 LLP  
</strong>2150 Allston Way Ste 400  
Berkeley, CA 94702  
USA  
Tel: +1 510 981-3110  
Email: <joe@mod4llp.com>

<strong>Michael Mount, Solo Practitioner  
Law Office of Michael P. Mount</strong>  
382 Granite Creek Road  
Santa Cruz, CA 95065  
USA  
Tel: +1 (831) 421-9115   
Email: <mmount@pacbell.net>

<strong>Angela Nolan, Owner  
Nolan Law  
</strong>6477 Little Falls Drive  
San Jose, CA 95120  
USA  
Tel: +1 408 203-3871  
Email: <anolan@angelanolanlaw.com>  
<strong>  
John Pavolotsky  
Greenberg Taurig LLP </strong>  
153 Townsend St. 8th Floor  
San Fransisco, CA 94107  
USA  
Tel: +1 (415) 655-1315  
Email: <pavolotskyj@gtlaw.com>

<strong>Mark Radcliffe, Partner  
DLA Piper  
</strong>2000 University Avenue  
Palo Alto, CA 94304-2214  
USA  
Tel: 1+ (650) 833-2266  
Email: <Mark.radcliffe@dlapiper.com>

<strong>Joel Riff, Attorney  
Joel Riff, Attorney at Law</strong>  
858 Northampton  
Palo Alto, CA 94303  
USA  
Tel: 1+ (650) 814-1845  
Email: <Joel.riff@yahoo.com>

<strong>Robert Rose, Attorney  
Loza & Loza LLP  
</strong>305 N. Second Ave. \#127  
Los Angeles, CA 91761  
USA  
Tel: +1 626 737-9036  
Email: <robert.rose@lozaip.com>

<strong>Suzanne Springs, General Counsel  
Celestial Holdings LLP</strong>  
403 Camberly Way Suite 200   
Redwood City, CA 94061  
USA  
Tel: 1+ (650) 780-9980  
Email: <suzanne@springslegal.com>

<strong>Joel Riff, Attorney at Law  
Royse Law Firm, PC  
</strong>858 Northampton  
Palo Alto, CA 94303  
USA  
Email: <joel.riff@yahoo.com>

<strong>Norman Van Treeck, Attorney  
Sheldon Mak Rose & Anderson</strong>  
100 Corson Street    
Pasadena, CA 91103  
USA  
Tel: 1+ (626) 796-4000  
Email: <nvantreeck@usip.com>

<strong>Warren Dranit, Attorney  
Spaulding, McCullough & Tansil LLP  
</strong>90 South E Street  
Suite 200  
Santa Rosa, CA 95404  
USA  
Email: <dranit@smlaw.com>

<strong>Christopher Stevenson  
Sullivan & Worchester LLP</strong>  
1058 Blackfield Court  
Santa Clara, CA 95051  
USA  
Email: <cstevenson72@gmail.com>

<strong>David Wang, Partner  
Wilson Sonsini Goodrich & Rosati </strong>  
650 Page Mill Road  
Palo Alto, CA 94304  
USA  
Tel: 1+ (650) 493-9300  
Email: <dwang@wsgr.com>

<hr width="100%" size="2" />
<h1>
<strong>Colorado</strong>

</h1>
<strong>Eric Kessner, Attorney and Counselor at Law  
Business, Technology, and Intellectual Property  
The Kessner Law Firm LLC  
</strong>1877 Broadway, Suite 704  
Boulder, CO 80302  
USA  
Tel: (303) 532-5780  
Email: <emk@kessnerlaw.com>

<strong>Pam O'Mara, President  
IP-InSource LLC</strong>  
8200 S Quebec Street Ste A3-296  
Centennial, CO 80112-3194  
USA  
Tel: + 1 (720) 839-8104    
Email: <pom@ipinsource.com>

<hr width="100%" size="2" />
<h1>
<strong>Connecticut</strong>

</h1>
<strong>Brendan Witherell, Associate  
Robinson & Cole, LLP  
</strong>280 Trumbull Street  
Hartford, CT 06103  
USA  
Email: <bwitherell@rc.com>

<hr width="100%" size="2" />
<h1>
Florida

</h1>
<strong>Albert Maggio, Of Counsel  
Carey Rodriguez Greenberg & Paul, LLP  
</strong>950 Peninsula Corporate Circle  
Suite 2022  
Boca Raton, FL 33487  
USA  
Tel: 1+ (561) 922-3836  
Email:<amaggio@crgplaw.com>

<hr width="100%" size="2" />
<h1>
Georgia

</h1>
<strong>Luke Anderson, Partner  
Atlanta Technology Law  
</strong>3495 Piedmont Ave.  
Atlanta GA, 30305  
USA  
Email: <landerson@atltechlaw.com>

<strong>Brandon Woods, Attorney  
Nelson Mullins Riley & Scarborough LLP  
</strong>Atlantic Station  
201 17th Street NW  
Suite 1700  
Atlanta, GA 30363  
USA  
Tel: +1 404 322-6462  
Email: <brandon.woods@nelsonmullins.com>

<hr width="100%" size="2" />
 

<h1>
Illinois

</h1>
<strong>Jean Benz, Attorney</strong><strong>  
Axiom Law</strong>  
33 West Monroe Street  
Chicago, IL 60603  
USA  
Tel: + 1 (312) 528-7600  
Email:<jean.benz@axiomlaw.net>

<strong>Alan Wernick,</strong><strong> Partner  
FSB FisherBroyles, LLP</strong>  
Chicago, IL 60606  
USA  
Tel: + 1 (847) 786–1005  
Email:[WERNICK@FSBLEGAL.COM](mailto:wernick@fsblegal.com)

<strong>David Foley, Attorney</strong><strong>  
Kirkland & Ellis </strong>  
300 N. Lasalle St.  
Chicago, IL 60654  
USA  
Tel: + 1 (312) 862-2749  
Email:<david.foley@kirkland.com>

<strong>Daniel Schaeffer,</strong><strong> Of Counsel  
Neal & McDevitt LLC</strong>  
1776 Ash Street  
Northfield, IL 60093  
USA  
Tel: + 1 (847) 881-2461   
Email:<dschaeffer@nealmcdevitt.com>

<hr width="100%" size="2" />
<h1>
Maine

</h1>
<strong>Robert Waeldner,</strong><strong> Attorney  
Waeldner Law Offices</strong>  
298 Main Street  
Yarmouth, ME 04096  
USA  
Email: <rw@waeldnerlaw.com>

<hr width="100%" size="2" />
<h1>
<strong>Maryland</strong>

</h1>
<strong>Barbara Berschler,</strong><strong> Attorney  
Press Potter & Dozier, LLC</strong>  
7910 Woodmond Ave. \#1350  
Suite 1350  
Bethesda, MD 20814  
USA  
Tel: + 1 (301) 913-5200  
Email:<bberschler@presspotterlaw.com>

<strong>David Moore,</strong><strong> Patent Attorney  
Rahman LLC </strong>  
10025 Governor Warfield Parkway  
Suite 110  
Columbia, MD 21044  
USA  
Email: <moore@rahmanllc.com>

<hr width="100%" size="2" />
<h1>
<strong>Massachusetts</strong>

</h1>
<strong>Ralph Bakkensen, Licensing & Contracts Attroney  
Law Firm of Ralph Bakkensen  
</strong>27 Blueberry Hill Road  
Andover, MA 01810  
USA  
Tel: 1 (978) 749-9828   
Email:<ralph@bakkensen.com>

<strong>Robert Blasi, IP Attorney   
Goodwin Procter LLP  
</strong>53 State Street  
Boston, MA 02109  
USA  
Tel: 1+ (617) 570-1408  
Email:<rblasi@goodwinprocter.com>

<strong>Kathleen Chapman, Patent Attorney   
Burns & Levinson LLP  
</strong>125 Summer St.  
Boston, MA 02110  
USA  
Tel: 1+ (617) 345-3694  
Email:<Kchapman@burnslev.com>

<strong>Karen Copenhaver, Attorney  
Choate Hall & Stewart  
</strong>Two International Place  
Boston, MA 02110  
USA  
Email: <kfc@choate.com>

<strong>Deborah Dong, Attorney  
Law Offices of Deborah B. Dong, LL.M.  
</strong>20 Park Plaza, Suite 400  
Boston, MA 02116  
USA  
Tel: 1+ (617) 412-7210  
Email: <deborah@deborahdonglaw.com>

<strong>Therese Hendricks, Partner  
Rissman, Hendricks & Oliverio  
</strong>100 Cambridge St.  
Boston, MA 02114  
USA  
Tel: 1+ (978) 289-1219  
Email: <Thendricks@rhoiplaw.com>

<strong>Shabbi Khan, Associate Attorney  
Burns & Levinson, LLP  
</strong>125 Summer St,  
Boston, MA 02110  
USA  
Tel: +1 (617) 345-3565  
Email: <skhan@burnslev.com>

<strong>Henry Knoblock, Partner  
Seegel, Lipshutz & Wilchins LLP  
</strong>20 William Street  
Suite 130  
Wellesley, MA 02481  
USA  
Tel: +1 781 237-4400  
Email: <hknoblock@slwllp.com>  
<strong>  
Cynthia Larose, Attorney  
</strong>Popeo PC  
One Financial Center  
Boston, MA 2111  
USA  
Email: <cjlarose@mintz.com>  
  
<strong>Orlando Lopez, Partner    
</strong>Burns & Levinson LLP  
125 Summer St.  
Boston, MA 02110  
USA  
Email: <olopez@burnslev.com>  
  
<strong>Robin Low, Attorney  
</strong>Robin P. Low, Esq.  
70 Atlantic Road  
Swampscott, MA 01907  
USA  
Tel: 1+ (781) 632-6859  
Email: <Robinesq1@verizon.net>  
  
<strong>Peter Moldave, Partner  
Gesmer Updegrove LLP  
</strong>40 Broad Street  
Boston, MA 02109  
USA  
Tel: 1+ (617) 350-6800  
Email:<peter.moldave@gesmer.com>  

<strong>Howard Novick, Principal  
Law Office of Howard Novick</strong>  
6 Park Road  
Sharon, MA 02067  
USA  
Tel: 1+ (781) 354-7431  
Email:<howardnovick9@gmail.com>

<strong>Robert Schwartz, Counsel  
Devine Millimet & Branch </strong>  
300 Brickstone Square  
Andover, MA 01810  
USA  
Tel: 1+ (978) 289-1219  
Email: <Rschwartz@devinemillimet.com>

<hr width="100%" size="2" />
<h1>
Michigan

</h1>
<strong>Charles E. Frayer, JD, MS</strong>  
16715 Anderson Drive  
Southgate, MI 48195  
United States  
Tel: 313-318-7705  
Email: <cefrayer@gmail.com>

<hr width="100%" size="2" />
<h1>
Minnesota

</h1>
<strong>Mike Cohen, Sr. Counsel  
Lawson Software, Inc. </strong>  
380 Saint Peter Street  
St. Paul, MN 55102  
USA  
Email: <mike.cohen@lawson.com>

<strong>John Schrager, Attorney  
Wagner, Falconer Judd </strong>  
1700 IDS Center  
80 S. 8th Street  
Minneapolis, MN 55402  
USA  
Tel: (612) 339-1421  
Email: <jschrager@wfjltd.com>

<hr width="100%" size="2" />
<h1>
Missouri

</h1>
<strong>Jason Schwent, Associate  
Thompson Coburn LLP</strong>  
One US Bank Plaza  
St. Louis, MO 63101  
USA  
Tel: (314) 552-6291  
Email: <Jschwent@thompsoncoburn.com>

<hr width="100%" size="2" />
<h1>
New Jersey

</h1>
<strong>Morris Greb, Attorney-at-Law  
Morris Leo Greb, Attorney-at-Law</strong>  
141 West Main Street  
Rockaway, NJ 07866  
USA  
Tel: 1+ (973) 983-9700  
Email: <mlg@greblaw.net>

<hr width="100%" size="2" />
<h1>
New York

</h1>
<strong>Charles Kramer, Attorney  
Charles Kramer</strong>  
200 East 10th Street, No. 816  
New York, NY 10003  
USA  
Email: <charles@charlesbkramer.com>

<strong>Mena Kaplan  
Paul, Weiss, Rifkind, Wharton and Garrison LLP</strong>  
1285 Avenue of the Americas  
New York, NY 10019-6064  
USA  
Tel: 1+ (212) 373-3562  
Email: <menakaplan@paulweiss.com>

<strong>Shaomei Ruan, Associate  
Rosensteel Law</strong>  
90 Park Avenue  
New York, NY 10016  
USA  
Tel: 1+ (212) 808-8300  
Email:<sruan@rosensteellaw.com>

<strong>Minjung Suh, Attorney  
Rosensteel Law</strong>  
90 Park Avenue  
New York, NY 10016  
USA  
Email: <msuh@rosensteellaw.com>

<strong>Alexandra Wong, Attorney  
Rosensteel Law  
</strong>90 Park Avenue  
New York, NY 10016  
USA  
Tel: 1+ (212) 808-8300  
Email:[acwong@rosensteellaw.com ](mailto:acwong@rosensteellaw.com)

<hr width="100%" size="2" />
<h1>
North Carolina

</h1>
<strong>Jessica Kruger, Attorney  
O'Hanlon & O'Hanlon </strong>  
2701 Glenwood Gardens Ln \#306  
Raleigh, NC 27608  
USA  
Email: <Jessica@ohanlonlaw.com>

<strong>Suzanne O'Hanlon, Lawyer  
O'Hanlon & O'Hanlon</strong>  
120 Livingstone Dr.  
Cary, NC 27513  
USA  
Email: <Suzanne@ohanlon.com>

<hr width="100%" size="2" />
<h1>
Ohio

</h1>
<strong>Ria Schalnat, Counsel  
Frost Brown Todd  
</strong>201 E. 5th St.  
Cincinnati, OH 45014  
USA  
Tel: +1 513 651-6426  
Email: <rschalnat@fbtlaw.com>

<hr width="100%" size="2" />
 

<h1>
Oregon

</h1>
<strong>Vicki Ballou, Partner  
Tonkon Torp LLP  
</strong>888 SW Fifth Avenue, Suite 1600  
Portland, Oregon 97204  
USA  
Tel: 1+ (503) 221-1440  
Email: <vicki.ballou@tonkon.com>

<strong>Rosemary Colliver, Associate  
Tonkon Torp LLP</strong>  
888 SW Fifth Avenue, Suite 1600  
Portland, Oregon 97204  
USA  
Tel: 1+ (503) 802-2050  
Email: <rosemary.colliver@tonkon.com>

<hr width="100%" size="2" />
<h1>
Pennsylvania

</h1>
<strong>Gerry Elman, President  
Elman Technology Law, PC</strong>  
PO BOX 209  
Swarthmore, PA 19081  
USA  
Tel: 1+ (610) 892-9942  
Email:<elman@elman.com>

<strong>Ethan Stone, Principal  
Stone Business Law</strong>  
1210 Colonial Ct.  
Norristown, PA 19403  
USA  
Tel: 1+ (650) 318-5732  
Email: <ethan.stone@stonebusinesslaw.com>

<strong>Vlad Tinovsky  
Tinovsky Law Firm, P.C. </strong>  
Two Liberty Place 22nd Floor  
50 S. 16th St.  
Philadelphia, PA  
USA  
Tel: 1+ (215) 568-6860  
Email: <vtinovsky@tinovsky.com>

<hr width="100%" size="2" />
<h1>
<strong>South Carolina</strong>

</h1>
<strong>John Harleston, Attorney  
The Harleston Law Firm, LLC  
</strong>P.O. Box 2187  
Mt. Pleasant, SC 29464  
USA  
Tel: +1 (843) 971-9453  
Email: <john@harlestonlawfirm.com>

<h1>
<strong>Texas</strong>

</h1>
<strong>Matthew Colagrosso, IP Associate  
Baker Botts  
</strong>2001 Ross Ave.  
Dallas, TX 75201-2980  
USA  
Tel: +1 (214) 953-6631  
Email: <matthew.colagrosso@bakerbotts.com>

<strong>Kem McClelland, Attorney  
Law Offices of Kem McClelland  
</strong>4504 Finley Dr  
Austin, TX 78731  
USA  
Tel: +1 (512) 658-6816  
Email: <Kem.mcclelland@gmail.com>

<strong>Stephen Stein, Partner  
Thompson & Knight, LLP  
</strong>1722 Routh Street, Suite 1500  
Dallas, TX 75230  
USA  
Tel: +1 (214) 969-1209  
Email: <steins@tklaw.com>

<strong>Jackquelyn Strickland, Managing Partner  
The Strickland Law Firm PC  
</strong>235 Peachtree St NW, Suite 400  
Dallas, TX 75240  
USA  
Tel: +1 (972) 934-6580  
Email: <js@thestricklandlawfirm.com>

<strong>Andrew Tekippe, Legal Counsel  
Snrlabs Corporation  
</strong>2505 N. Plano Road  
Suite 2300  
Richardson, TX 75082  
USA  
Email: <Andrew@snrlabs.com>

<strong>Jeff Tinker, Attorney  
Winstead PC  
</strong>1201 Elm St  
Dallas, TX 75270  
USA  
Tel: +1 (214) 745-5346   
Email: <jtinker@winstead.com>

<hr width="100%" size="2" />
<h1>
<strong>Utah</strong>

</h1>
<strong>Stirling Adams, Attorney  
Novell </strong>  
1800 South Novell Place  
Provo, UT 84606  
United States  
Tel: +1 (801) 861-6907  
Email: <sadams@novell.com>

<hr width="100%" size="2" />
<h1>
Virginia

</h1>
 

<strong>David Charles, Shareholder  
Rees Broome, PC  
</strong>8133 Leesburg Pike  
Ninth Floor  
Vienna, VA 22182  
USA  
Tel: +1 703 790-1911  
Email: <dcharles@reesbroome.com>

<strong>Nora Garrote, Partner  
Venable LLP  
</strong>8010 Towers Crescent Drive  
Suite 300  
Vienna, VA 22182  
USA  
Tel: +1 703 760-1983  
Email: <negarrote@venable.com>  
  
<strong>Daniel Gropper, Patent Attorney  
Daniel R. Gropper, PC  
</strong>9908 Dale Ridge Ct.  
Vienna, VA 22181  
USA  
Tel: +1 703 281-7457  
Email: <dgropper@drglaw.net>  
<strong>  
Walter Kubitz, Attorney  
Litman Law</strong>  
8955 Center St.  
Manassas, VA 20110  
USA  
Tel: 1+ (703) 486-1000  
Email: <wkubitz@litmanlaw.com>

<hr width="100%" size="2" />
<h1>
<strong>Washington</strong>

</h1>
<strong>Mark Wittow, Partner  
K&L Gates </strong>  
925 4th Ave., Suite 2900  
Suite 2900  
Seattle, WA 98144  
USA  
Tel: +1 (206) 370-8399  
Email:<mark.wittow@klgates.com>

<hr width="100%" size="2" />
<h1>
<strong>Washington, DC</strong>

</h1>
<strong>Shayne O'Reilly, Intellectual Property Attorney  
Kilpatrick Townsend & Stockton</strong>  
607 14th Street NW  
Washington, DC 20005  
USA  
Tel: +1 (202) 639-4722  
Email:<soreilly@kilpatricktownsend.com>

<strong>Martin Zoltick, Officer and Director  
Rothwell, Figg </strong>  
1425 K St., NW  
Washington, DC 20005  
USA  
Tel: +1 (202) 783-6040  
Email:<mzoltick@rfem.com>

<hr width="100%" size="2" />
<h1>
Wisconsin

</h1>
<strong>Mr. Daniel Hanrahan  
Andrus, Sceales, Starke & Sawall, LLP </strong>  
100 East Wisconsin Ave, Suite 1100  
Milwaukee, WI 53202  
USA  
Tel: +1 (414) 271-7590  
Email: <dhanrahan@andruslaw.com>

<strong>Mr. Christopher Scherer  
Andrus, Sceales, Starke & Sawall, LLP </strong>  
100 East Wisconsin Ave  
Suite 1100  
Madison, WI 53202  
USA  
Tel: +1 (414) 271-7590  
Email:<cscherer@andruslaw.com>

<strong>Marc Adesso, Attorney  
DeVos & Zerbst, S.C.</strong>  
2000 Engel Street  
Suite 101  
Madison, WI 53713  
USA  
Email: <marc@devosandzerbst.com>

<strong>Amy Arndt, Attorney  
Reinhart Boerner Van Deuren </strong>  
1000 N. Water St. Suite 2100  
Milwaukee, WI 53202  
USA  
Tel: +1 (414) 298-8300  
Email: <aarndt@reinhartlaw.com>

<strong>Elizabeth Russell, Attorney  
Russell Law </strong>  
49 Kessel Court  
Suite 200  
Madison, WI 53711  
USA  
Tel: +1 (608) 285-5007  
Email:<beth@erklaw.com>
