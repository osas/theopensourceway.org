---
title: The Open Source Way
permalink: wiki/The_Open_Source_Way/
redirect_to: /wiki/The_Open_Source_Way:_Creating_and_nurturing_communities_of_contributors/
---

You should automatically be redirected to [The Open Source Way: Creating and nurturing communities of contributors](/wiki/The_Open_Source_Way:_Creating_and_nurturing_communities_of_contributors/)
