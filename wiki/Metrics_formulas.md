---
title: Metrics formulas
permalink: wiki/Metrics_formulas/
layout: wiki
tags:
 - Metrics working group
---

Evangelism rating
-----------------

How much evangelism is happening around a project?

This formula gives a measure based on the number of talks by a number of
people by a number of conferences over time:

    tpc/T

-   Number of talks = t
-   Number of people = p
-   Number of conferences = c
-   Time = T

