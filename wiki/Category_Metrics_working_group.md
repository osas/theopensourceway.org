---
title: Category:Metrics working group
permalink: wiki/Category:Metrics_working_group/
layout: tagpage
tag: Metrics working group
---

''Short URL: <http://bit.ly/MetricsWG> ''

Mission
-------

**The mission of the group is to create methods for tracking community
health.**

### ... and that means?

Many people and organizations have different *goals*, and therefore
different reasons for tracking community health.

This working group is a common location for those people to share tools,
methods, processes, techniques, reasons, code, and so forth. Anything
that we have, that we can and want to share as free and open source and
content.

You can just lurk. You can share whatever you want.

All of us may have silos of tools and information, where the data is
openly accessible, but the interwoven connections and meanings are
unique to your community.

This working group is where we pull out whatever we can from that silo
and see if we can grow and improve it together.

Ultimately, this is a [community of
practice](/wiki/Communities_of_practice "wikilink") around community healthy
metrics. We decide what is important to us.

### Goals of the working group

#### To start

Initial actions:

-   Join <http://lists.theopensourceway.org/mailman/listinfo/metrics-wg>
-   Look at sentiment analysis software we might use.
-   Use <http://piratepad.net/tosw-metrics-wg-initial-invites>
-   See what academia is doing

What to work on NOW:

-   Get your own instance of what Meego has done up and running.
-   Start collecting data now, some of the data is not kept over time.
    -   Especially for looking at lurkers.
-   Invite people to join the working group.

#### Long term

"A magic dashboard and back-end tools that show me in real time what the
health of my community is. These views are hard data and trend-based,
run through intelligence that I help craft for my unique community, and
automatically alert me at tolerance levels I set. If someone is having
problems somewhere in our community, it should appear here."

Working group communication
---------------------------

-   [Mailing list
    metrics-wg@theopensourceway.org](http://lists.theopensourceway.org/mailman/listinfo/metrics-wg)
-   1.  metrics-wg on irc.freenode.net

-   

GSoC Mentor Summit session
--------------------------

-   [Notes](https://gsoc-wiki.osuosl.org/index.php/Metrics_working_group)
    -   Local mirror of those notes at [Metrics working group session at
        the GSoC Mentor Summit
        2011](/wiki/Metrics_working_group_session_at_the_GSoC_Mentor_Summit_2011 "wikilink").
-   Pictures (coming)

