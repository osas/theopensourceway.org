---
title: Data and references
permalink: wiki/Data_and_references/
layout: wiki
tags:
 - The Open Source Way book
---

This chapter contains copies or links to data and references used in
this book.

References and recommended books and articles
---------------------------------------------

These are online and offline references that support conclusions drawn
elsewhere.

-   [Interview with Seneca College's David
    Humphrey](http://pyre.third-bit.com/blog/archives/2006.html), where
    he gives a succinct list of success factors.
-   [Josh Berkus' Ten Ways to Destroy Your
    Community](http://it.toolbox.com/blogs/database-soup/community-destroyers-24309),
    with some replies to Rodrigues' rebuttal (below.)
    -   [Colin Charles post on the
        presentation](http://www.bytebot.net/blog/archives/2008/05/07/ten-ways-to-destroy-your-community)
    -   [Zack Urlocker post on the
        presentation](http://weblog.infoworld.com/openresource/archives/2008/05/josh_berkus_on.html)
    -   [Savio Rodrigues rebuttal post on the
        presentation](http://weblog.infoworld.com/openresource/archives/2008/05/questioning_jos.html)
    -   [Jonathan Corbet post on LCA 2010
        version](http://lwn.net/Articles/370157/)
-   [The top ten
    rules](http://en.wikipedia.org/wiki/The_Starfish_and_the_Spider#The_Ten_Rules)
    from [The Starfish and The
    Spider](http://en.wikipedia.org/wiki/The_Starfish_and_the_Spider).
-   [Open Source Triple
    Play](http://www.redhat.com/magazine/001nov04/features/tripleplay/)
-   [How to get your code into an open source
    project](http://et.redhat.com/~rjones/how-to-supply-code-to-open-source-projects/)
-   [Dreamwidth - Build Your Own Contributors, One Part At A
    Time](http://www.slideshare.net/dreamwidth/build-your-own-contributors-one-part-at-a-time)
-   [How Open Source Projects Survive Poisonous
    People](http://www.youtube.com/watch?v=ZSFDm3UYkeE) is the first,
    and a later version [How to Protect Your Open Source Project From
    Poisonous People](http://www.youtube.com/watch?v=-F-3E8pyjFo), are
    talks that are applicable to any community drawing from many years
    experience as open source developers.
-   [Keynote: Dan Frye - 10+ Years of Linux at
    IBM](http://video.linuxfoundation.org/video/1709) from the 2010
    Linux Foundation Collaboration Summit.
-   [dojo foundation](http://www.dojofoundation.org/about/hundredpoint/)
    100-point open source projects method.
-   [Groklaw on trolls, astroturfers, and the open source
    way](http://www.groklaw.net/article.php?story=20100614034659206) is
    an *excellent* story.
-   [Build an authentic, valuable online
    community](http://opensource.com/business/10/5/build-authentic-valuable-online-community#comment-1731)
    tells a bunch of good stories and is accessible to diverse audiences
    for learning about applying the open source way to their online
    community building.
-   [10 easy ways to attract women to your free software
    project](http://www.freesoftwaremagazine.com/columns/ten_easy_ways_attract_women_your_free_software_project)
    is a very useful and informative article, with approaches that are
    for any type of contributor and project.
-   [How to work
    open](http://openmatt.wordpress.com/2011/04/06/how-to-work-open/) is
    a short and useful introduction derived from a Mozilla team
    workshop.
-   [Open Advice](http://open-advice.org/) is a knowledge collection
    from a wide variety of Free Software projects. It answers the
    question what 42 prominent contributors would have liked to know
    when they started so you can get a head-start no matter how and
    where you contribute.
-   [A few thoughts on open projects, with mention of
    Scala](http://blog.ometer.com/2012/03/15/a-few-thoughts-on-open-projects-with-mention-of-scala/)
    is an article from Havoc Pennington that explores aspects of
    successful open projects and how they are affected by paid
    organizational focuses.
-   [Unsecret Source: Marketing strategy the open source
    way](http://www.johnderry.com.au/76/unsecret-source-marketing-strategy-open-source-way)
    is a short article about the principles of open marketing, paired
    with an actually open marketing plan for
    [CiviCRM](http://civicrm.org).
-   [Enabling students in a digital age: Charlie Reisinger at
    TEDxLancaster](https://www.youtube.com/watch?v=f8Co37GO2Fc) is a
    video describing a project to provide FOSS laptops for students, and
    how/why that really *really* **really** matters.

Online community resources
--------------------------

-   [Community Management
    Wiki](http://communitymgt.wikia.com/wiki/Community_Management_Wiki)

(List of books from [Books you should
read](/wiki/Books_you_should_read "wikilink").)

Data
----

This section contains copies of relevant data for immediate reference,
as well as links and sources.
