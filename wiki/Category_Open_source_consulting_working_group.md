---
title: Category:Open source consulting working group
permalink: wiki/Category:Open_source_consulting_working_group/
layout: tagpage
tag: Open source consulting working group
---

*(Short URL: <http://bit.ly/OSSConsultingWG> )*

Community Charter
-----------------

This working group provides a place for various organizations involved
in Open Source consulting to collaborate on a framework of common
intellectual property that benefits all consumers. For example, open
source '101-level' training materials, community health metrics, etc.
Much like organizations utilize common software components from the open
source software world (and contribute back to them), we believe there is
value to these organizations participating in creating
non-differentiated intellectual property around how to consume,
collaborate, and create in the open source world, while still being able
to differentiate their own consulting services with unique offerings,
tools, and industry domain expertise.

The ultimate goal is that, in addition to providing common training
materials and metrics, this community will define standards and
practices in governance models and other areas that help organizations
identify and utilize open source more effectively in their enterprises.
By cooperating on these common pieces, while providing custom
value-added services utilizing this 'framework', the overall consulting
market for open source should expand as potential consumers recognize
the benefit of maturity and commonality in approach, offering
opportunity and value for all parties involved.

Potential Collaboration Areas
-----------------------------

The following is not an exhaustive list, and the community will need to
discuss what other areas it would like to see covered:

-   [Metrics](https://www.theopensourceway.org/wiki/Metrics_working_group)
    (existing working group already started)
    -   Open Source Project Maturity/Health Index
-   [Legal
    References/Referrals](https://www.theopensourceway.org/wiki/OSS_CWG_Legal_Referrals)
    (End goal - 'rate-able' like Angie's List)
-   Open Source Primers/Training
-   Common Reporting Templates

License Terms
-------------

**CC-BY-SA**

Community Membership/Participants/Contributions
-----------------------------------------------

Membership in this community is open to any individual or organization
that would like to contribute. While Red Hat is launching the community,
we encourage others to join & help lead as well.

This community is in it's infancy, however, the following community
member companies/individuals have either contributed content or have
agreed in principle to do so.

-   Red Hat
    -   Training modules (in process)
    -   Maturity assessments (in process)
    -   [Metrics](https://www.theopensourceway.org/wiki/Metrics_working_group)
-   Olliance Group (A Black Duck Company)
    -   [Legal
        References/Referrals](https://www.theopensourceway.org/wiki/OSS_CWG_Legal_Referrals)
-   Linux Foundation
    -   Training modules (in process)
-   OpenTech Strategies (Karl Fogel)
    -   Best practices (in process)
-   OpenLogic (agreement to contribute - specifics in process)

Please email <guym@redhat.com> if you need an account on this wiki to
add your team or contributions.

Getting Started
---------------

-   Join the mailing list
    [oss-consulting-wg@theopensourceway.org](http://lists.theopensourceway.org/mailman/listinfo/oss-consulting-wg)
    -   Mailing list
        [archives](http://lists.theopensourceway.org/pipermail/oss-consulting-wg/)
-   Join our IRC channel: \#oss-cwg on irc.freenode.net
-   Introduce yourself in either of these forums & share what areas
    you'd like to work on from the list above (or propose your own)

Launch Efforts
--------------

The first phase of launching this community took place at the [Community
Leadership Summit](http://www.communityleadershipsummit.com/), held on
July 14th and 15th, 2012. Guy Martin, Open Source Consulting Lead at Red
Hat, [presented](:File:OSS-Consultants-UNITE-CLS.pdf "wikilink") a
plenary session with Dave Neary, Community Action/Impact Leader, Red Hat
Open Source and Standards, on Saturday to share our thoughts on why a
community like this is valuable.

We saw a lot of hands go up when Dave asked how many folks were
interested/had a need for the kinds of common components we are
proposing in this community. Additionally, we ran a session on this
topic on Sunday, and had some engaging discussions about why and how
people would contribute. Shawn Briscoe from Black Duck took some
excellent
[notes](http://communityleadershipsummit.wikia.com/wiki/Open_Source_Consultants_Unite)
. The upshot of the conversation seemed to be that individual private
consultants might not necessarily want to contribute content they saw as
value-added vs. corporate-backed consultants who believe a lot of the
components are commodity at this point. We respect the right of those
private consultants to possibly not participate in this community if it
doesn't fit their needs, but we are going to continue to push forward
here in figuring out what can logically be shared.
