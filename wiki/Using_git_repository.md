---
title: Using git repository
permalink: wiki/Using_git_repository/
layout: wiki
tags:
 - How to
 - Documentation
 - Contributing to TOSW
---

This document explains how to download the source, make changes, and
submit those changes back to the list.

Git is explained in much more detail in many places, but one of the
recommended sites is the [Git community book](http://book.git-scm.com).
The site includes easy explanations and helpful video tutorials.

Read the [Contribution policy](/wiki/Contribution_policy "wikilink") if you
are contributing to this wiki or book.

For instructions on building the book, read [Building using
Publican](/wiki/Building_using_Publican "wikilink").

Git briefing
------------

Git is a source code management (SCM) system that is extremely fast and
efficient, and also incredibly flexible. There is a small learning
curve, but most people master the basics very quickly.

Git has three main content areas you need to know about:

-   The *index*, which is stored in a special *.git* folder -- hidden
    because you normally don't need to interact with it yourself
-   The *stage*, which contains things that are due to be committed
-   The *working tree*, which are the files and folders you actually
    edit and work with

When you work with git, you edit things in the *working tree* in a
conventional manner. Then you add some or all of these changes to the
*stage*. This prepares them for a *commit*, a recordin of changes to the
last state of the project. Once you make a *commit*, the changes are
recorded in the *index*, and can later be *pushed* (synchronized) with
another git repository, such as the shared repository on the network
that your project contributors share.

Unlike older SCMs, git allows you to make commits locally which you can
later synchronize with the online repository, so that you can do useful
work even if you're not on a network. In the majority of cases, this
synchronization is easy even if the shared repository on the network has
had changes made while you were working offline. This makes git ideal
for collaborative projects.

Cloning the repository
----------------------

First, you *clone* the shared repository, which gives you a complete
copy of the repository. With this repository, you can work independently
whenever you need to.[1]

`git clone `[`git://git.fedorahosted.org/git/tosw.git`](git://git.fedorahosted.org/git/tosw.git)

This procedure creates a *tosw* folder in your current directory and
stores the content inside it.

Making a change
---------------

Edit files as necessary, using your favorite editor of choice.

**NOTE:** Take care to observe any project ground rules regarding
formatting &lt;&lt;<LINK?>&gt;&gt;, so that you do not make frivolous
changes. For example, reformatting a whole file so that all the initial
spaces turn into tabs (or vice versa) would cause a long set of
line-by-line changes that obscure your meaningful changes.

### When is the right time to stage changes?

A set of changes should be a single topic. Here are some good examples
of changes after which you'd want to stage changes:

-   Added a section of text to better explain a specific concept
-   Changed a misspelling that only happens in one place
-   Added a new chapter

Don't mix up a bunch of unrelated changes at once, and don't wait too
long before you stage your changes. Staging and committing are very
quick processes that won't get in the way of your work. If you wait too
long and pile up a bunch of unrelated changes, it will be harder to sort
them out, especially if you've made unrelated changes to the same
file.[2]

Staging the change
------------------

Cover:

-   What the stage does
-   git add
-   Being careful not to lose staged changes

Committing the change
---------------------

Your commit message explains the change you made for the benefit of
others working on the project. In general, a commit should be
*self-contained* and where possible *self-explanatory*. Don't mix up a
bunch of unrelated changes in a single commit.

Email or push?
--------------

### Emailing your patches

One of the main ways of contributing to source is to use a *patch*,
which is a file that shows just the differences between to text files,
so the changes from a second file can be made to the first file. A patch
can be sent to a mailing list for code/content review and acceptance.
You can learn more about patches from the chapter [Fixing the
Code](http://teachingopensource.org/index.php/Fixing_the_Code).

Using `git format-patch` creates a patch of your changes based on the
last changes in the `master` branch. The command `git send-email`
formats and emails the patch to a mailing list. Read more about using
`git format-patch` and `git send-email`:

<http://www.kernel.org/pub/software/scm/git/docs/git-format-patch.html>

<http://book.git-scm.com/5_git_and_email.html>

You can submit a patch to the [mailing
list](http://lists.fedorahosted.org/mailman/listinfo/tosw) at any time
without any special permission or authorization.

### Pushing your changes

We're interested in giving you commit access to the repository and we'd
like to know a bit more about you. When you are ready to start
committing to the main `git` source repository, send an email to the
\[<http://lists.fedorahosted.org/mailman/listinfo/tosw> mailing list\].
Tell us who you are, what you want to do in the project, and ask any
questions you might have.

Cover:

-   pushing to an existing branch
-   pushing a new branch

------------------------------------------------------------------------

<references/>

[1] A clone of a git repository is *truly* complete. There is nothing
special about the shared location, other than that the project members
agree to use it as the central repository to which they will synchronize
all their changes. If the server where that repository resides explodes,
very little will be lost as long as someone, somewhere, has a recent
clone.

[2] Actually, git has functions for dealing with this too, since it
happens so often. But teaching you all about those functions is beyond
the scope of this page -- check out the [Git community
book](http://book.git-scm.com) for more information.
