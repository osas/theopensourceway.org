---
title: How to teach the open source way - workshop FUDCon Tempe 2011
permalink: wiki/How_to_teach_the_open_source_way_-_workshop_FUDCon_Tempe_2011/
layout: wiki
tags:
 - Events
---

This is a planning page for a workshop for [FUDCon Tempe
2011](http://fedoraproject.org/wiki/FUDCon:Tempe_2011).

*Status: Proposing workshop to SCALE organizers.*

Event details
-------------

**Workshop coordinators:**

-   [Karsten 'quaid' Wade](/wiki/User%3AQuaid "wikilink")
-   

### When

-   TBD

### Where

-   TBD

Curriculum
----------
