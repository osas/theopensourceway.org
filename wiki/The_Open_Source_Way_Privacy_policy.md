---
title: The Open Source Way:Privacy policy
permalink: wiki/The_Open_Source_Way:Privacy_policy/
layout: wiki
tags:
 - Legal
---

This site is covered by a privacy policy identical to the one found
here:

<http://fedoraproject.org/wiki/Legal:PrivacyPolicy>
