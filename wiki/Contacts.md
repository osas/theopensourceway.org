---
title: Contacts
permalink: wiki/Contacts/
layout: wiki
tags:
 - Communication
---

Contact anyone related to this project via the project mailing list:

<https://lists.fedoraproject.org/mailman/listinfo/tosw> (**pending**)

([Requested](https://fedorahosted.org/fedora-infrastructure/ticket/1986#preview)
