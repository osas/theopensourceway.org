---
title: The Open Source Way:Copyrights
permalink: wiki/The_Open_Source_Way:Copyrights/
layout: wiki
---

The text of and illustrations in this wiki are licensed by Red Hat and
others under a Creative Commons Attribution-Share Alike 3.0 Unported
license ("CC-BY-SA"). An explanation of CC-BY-SA is available at
<http://creativecommons.org/licenses/by-sa/3.0/>. The original authors
and licensors of this content designate TheOpenSourceWay.org as the
"Attribution Party" for purposes of CC-BY-SA. In accordance with
CC-BY-SA, if you distribute this document or an adaptation of it, you
must provide the URL for the original version.

Red Hat and other licensors of this content waive the right to enforce,
and agree not to assert, Section 4d of CC-BY-SA to the fullest extent
permitted by applicable law.

If you make a contribution to this wiki, including a talk/discussion
page, you agree to release the work under CC-BY-SA along with the waiver
and non-assertion specified in the preceding paragraph.

Content is copyright (c) 2009, 2010 Red Hat, Inc. and others.
