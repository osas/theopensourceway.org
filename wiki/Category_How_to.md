---
title: Category:How to
permalink: wiki/Category:How_to/
layout: tagpage
tag: How to
---

Pages that explain how to do something, usually related to contributing
to the project.
