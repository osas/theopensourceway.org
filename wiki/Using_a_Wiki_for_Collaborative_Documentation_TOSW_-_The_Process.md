---
title: Using a Wiki for Collaborative Documentation TOSW - The Process
permalink: wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_The_Process/
layout: wiki
tags:
 - Using a Wiki for Collaborative Documentation TOSW
---

-   Building a book with three chapters
    1.  Create a new page to be the top-level of the book by typing in
        the URL or making a stub link. The name should be descriptive,
        natural language, not CamelCase, and not redundant (e.g. adding
        Fedora to the name of a guide on fedoraproject.org.)
        -   [Using a Wiki for Collaborative Documentation
            TOSW](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW "wikilink")
    2.  Edit the new page. Introduce the book, scope, etc., then create
        a table of contents that are stub-links to the chapters.
        -   [Using a Wiki for Collaborative Documentation TOSW -
            Introduction](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Introduction "wikilink")
        -   [Using a Wiki for Collaborative Documentation TOSW -
            Standing on The Shoulders of
            Giants](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Standing_on_The_Shoulders_of_Giants "wikilink")
        -   [Using a Wiki for Collaborative Documentation TOSW - The
            Process](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_The_Process "wikilink")
    3.  Add it to at least one category immediately. It may have its own
        self-named category, too.
    4.  Create the second-level nested sections
        -   Each page is itself the first-level nesting
        -   Use Review to check for syntax mistakes
    5.  Save at this state because:
        -   Save often is safer, less work lost
        -   Once saved, you are more likely to share the URL
        -   Once saved, it shows up in the wiki recent changes list
            -   Release early, release often
        -   The work you save is the work others have to build on
            -   You may have reason to go suddenly, leave it so others
                can pick up where you left off.
    6.  Each sub-section can now be edited independently.
    7.  A common trick with wiki pages is to stub out all the sections.
        -   Gives an outline to work against.
        -   Makes it easy for others to work on part of the outline.
    8.  Collaborators dive in where they see fit and/or where agreed.
        For example, a few writers might work on a chapter together,
        each working on different sections, then cross-editing.
    9.  Using MediaWiki *version control* you can take a snapshot of a
        particular moment in a document, then continue editing the
        latest content. This is how TOS' 'Practical Open Source Software
        Exploration' worked.

