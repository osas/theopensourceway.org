---
title: Notes from CLS West session-20110115
permalink: wiki/Notes_from_CLS_West_session-20110115/
layout: wiki
tags:
 - CLS West
 - Session notes
---

(*Original EtherPad used was
<http://openetherpad.org/TOSW-CLS-West-session-20110115>*)

-   TOSW == "The Open Source Way" the book
-   tosw == the open source way, the methodology

Sharing ideas around for a while. FOSS has given legal framework, so,
yes, it is a new way of doing things.

The open source way is definitely a way to organize other groups, such
as non-profits.

When applying tosw, have to make accommodations for business needs.
Compare to non-proifits that can be radically transparent.

Important when in the middle of these extremes to review what you are
doing, looking for applying tosw wherever you can.

Tosw is more about how the community run than about what software is on
your infrastructure.

For example, hallway conversations create problems with the communities.
Tosw says, send something out to the mail public communication channel

Convincing non-profits to use FOSS for websites in a process of getting
them to think things through versus the ease of having some volunteer
whip out a website using a proprietary framework.

Is there a different approach to take when talking with non-profits?

"This is a model that is sustainable, and it's what you want to tie your
non-profit to."

"other than Apple and Microsoft, what other software companies are
around forever?"

"what other software can volunteers use without having to pay for the
skills?"

Organizing using open book principles means people understand and can
see the history of decision making, reducing finger-pointing/blaming in
the future when something inevitably goes wrong or comes under scrutiny.

Don't keep it secret that you have secrets. It's OK to have undiscussed
agenda items, but don't let people wonder if they exist or not.

Group is more empowered when decisions are spread to the group instead
of stuck on an individual.

Sometimes that makes it take longer for decisions. But trust level comes
in, we learn who can make good decisions and trust that person to
continue doing so for expediency and progress.

Having well defined processes helps people to know how things are done
so they can work the system efficiently. A group that meets regularly
and makes decisions means people are there, they decide, and the rhythm
is known.

Have enough procedure so people know their parameters.

An open, shared process means others can help improve it in seeing and
using it.

Acting excellently toward each other comes before governance.

World of Warcraft took it slow and watched the community before
developing their expansion packs, which is what makes them top of the
heap today. Everquest missed this, pushing out their own vision every 6
months, ignoring people, and pissing them off.

Many famous Twitter features were defined and worked up initialy from
the community, not just the developers. For example, the @convention and
the RT (re-tweet).

Tosw is about listening to people and making sure people are heard.

Throwing things in the open can be a way to get things done. It may come
from a low-resource need, but force multiplication also comes from the
fact that no single team can have all talents, skills, intelligence, and
experience. Many companies have noticed this by throwing up internal
wikis and having them become much more useful than ever expected,
turning in to full information codexes.

Free and open tools by their nature help people who are doing free and
open-type of work, such as non-profits. It's again about more than
no-cost, it's a fact that the models sync thus making it a
force-multiplier for the non-profits.

Co-working and co-housing are ways of living tosw.

Group housing has a lot of challenges. Have private plus community. What
can cities approve, etc. - so a condo-style fits. Look for ways it fits
other systems, how to leverage that to increase cooperation v.
competitiveness.

Help to fit the mental model of lenders and members. "It's just like
this, with ..." "It's just like that, with ..."

How to get people to know that it's not endless meetings, etc. The power
comes from not using the veto. We work at convincing each other,
compromising, etc. knowing that others can really make it hard. Looking
for consensus becomes the norm.

Co-working comes out of sharing, working as a community, instead of just
renting an office. Leaving code on a white board after a long meeting,
coming in the next day and finding it debugged.

Tosw way of life:

-   Trusting the information that needs to be shared is shared.
-   Group empowerment through decision making process
-   -   commuity of intent
-   transparency
-   sharing is caring

Need to be both a teacher and learner.

Internalizing can be a challenge, focus on making sure that people get a
chance to learn that. Learn that they can edit each other's work, etc.
Keep it simple.
