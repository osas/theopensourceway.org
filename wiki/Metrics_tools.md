---
title: Metrics tools
permalink: wiki/Metrics_tools/
layout: wiki
tags:
 - Metrics working group
---

These are single or bundled tools that people use when gathering,
processing, analyzing, and reporting on open community metrics.

It's most useful if the tools provided are open source themselves, or at
least provide a no-cost access. The goal is to enable people to use the
same tools to do their own work in their own communities.

Monitoring
----------

-   "Kosmos" the Komposite Open Source Monitoring Suite:
    <http://www.jboss.org/kosmos>
-   [Metrics Grimoire](http://metricsgrimoire.github.io/) - centralises
    and cross references information gathered from mailing lists
    (Mailing List Stats), bug trackers (Bicho), Git and Gerrit into one
    dashboard
    -   Used by OpenStack, Wikimedia Foundation; supported and developed
        by Bitergia
-   Pentaho-based dashboards
    -   Mostly bespoke, requires tooling for data gathering, some ETL to
        transform the data to a common cube, and reports to generate
        usable stats
    -   CDF, the Community Dashboard Framework, is a useful tool to
        use - developed by WebDetails, the Portugese company who
        developed the Mozilla metrics dashboard and Tizen metrics
        dashboard.
    -   More information on Pentaho-based dashboards and reporting on
        [the MeeGo wiki](http://wiki.meego.com/Metrics)

