---
title: Chapter writing methodology
permalink: wiki/Chapter_writing_methodology/
layout: wiki
tags:
 - How to
 - Documentation
 - Contributing to TOSW
---

*This chapter explains how to write chapters in [The Open Source
Way](/wiki/The_Open_Source_Way "wikilink") book.*

This book has a rhythm that is felt in the smallest sections and in the
overall structure: *principle, implementation, example*.

There are a few chapters that explain important principles, extracted in
a way to be applicable to a wide set of domains. These are currently:
[Communities of practice](/wiki/Communities_of_practice "wikilink"), [How to
loosely organize a
community](/wiki/How_to_loosely_organize_a_community "wikilink"), [Stuff
everyone knows and forgets
anyway](/wiki/Stuff_everyone_knows_and_forgets_anyway "wikilink"), [What your
organization does correctly when practicing the open source
way](/wiki/What_your_organization_does_correctly_when_practicing_the_open_source_way "wikilink"),
and [What your organization does wrong when practicing the open source
way](/wiki/What_your_organization_does_wrong_when_practicing_the_open_source_way "wikilink").

There are a few chapters that explain how to implement those principles
in other fields (domains). These currently are: [Business the open
source way](/wiki/Business_the_open_source_way "wikilink"), [Marketing the
open source way](/wiki/Marketing_the_open_source_way "wikilink"), [IT the open
source way](/wiki/IT_the_open_source_way "wikilink"), and [Law the open source
way](/wiki/Law_the_open_source_way "wikilink"), and [How to tell if a FLOSS
project is doomed to
FAIL](/wiki/How_to_tell_if_a_FLOSS_project_is_doomed_to_FAIL "wikilink").

There are a few chapters that show examples how individual
implementations of the principles have worked. These currently are:
[Great stories to tell](/wiki/Great_stories_to_tell "wikilink"), [Books you
should read](/wiki/Books_you_should_read "wikilink"), and [Data and
references](/wiki/Data_and_references "wikilink").

Within each of these chapters, the same rhythm is followed. In the
chapters on important principles, each section presents a single
principle, useful implementation information, and a brief example from
the real world of implementing the principle. Read [Section writing
methodology](/wiki/Section_writing_methodology "wikilink") for details of how
to use these elements within a chapter.

In the chapter [Great stories to
tell](/wiki/Great_stories_to_tell "wikilink"), each story has a list of links
back to the principles that are covered in the story, and links to the
implementation chapters where relevant.

Principle chapters
------------------

Ideally these remain limited in number, only adding an entirely new
chapter to cover a different perspective at this primary level.

Implementation chapters
-----------------------

The number of these chapters is limited by the number of domains of
human knowledge and practice.

An important part of these chapters is that they note where they deviate
or modify one of the primary principles.

Some domains may create new principles that don't apply as equally
outside of the domain. Those sort of principles ideally have an analogue
in the principles chapters, and if not, a close relationship.

Example chapters
----------------

These are all real world references in some way.

-   Specific reference materials;
-   Books to read;
-   Data resources;
-   Other scientific resources;
-   Anecdotes that demonstrate principle, implementation, and outcome;
-   Etc.

