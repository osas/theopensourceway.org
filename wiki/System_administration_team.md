---
title: System administration team
permalink: wiki/System_administration_team/
layout: wiki
tags:
 - System administration of theopensourceway.org
---

This website is run by a small, intrepid team seeking to practice the
open source way where it comes to administrating systems AKA sysadmin.

That means we'd love you to participate! You can even just lurk, learn,
comment, and hang out. No worries, you are welcome.

If you do want to help, we'll find some things you can do to prove your
skills before we give you the root or admin password to anything. ;-)

Members
-------

-   [Ian Weller](mailto:ian@ianweller.com)
-   [Karsten Wade](mailto:quaid@iquaid.org)

Communication
-------------

We hold a weekly IRC meeting:

-   Date: TBD
-   Location: \#opensource.com on Freenode IRC

We talk on our mailing list - [if it's not on the mailing list, it
doesn't
exist](/wiki/Stuff_everyone_knows_and_forgets_anyway#Take_extra_extra_extra_care_to_have_all_discussions_in_the_open "wikilink")!

<http://lists.theopensourceway.org/mailman/listinfo/infrastructure>
