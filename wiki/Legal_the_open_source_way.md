---
title: Legal the open source way
permalink: wiki/Legal_the_open_source_way/
layout: wiki
tags:
 - The Open Source Way book
---

*This is a stub page for content that needs to be created. Chapter scope
idea: review Groklaw article, summarize points, make a few good quotes,
and out to original in an admonition to read the original.*

*The following notes are a summary of how I got to the point of writing
this article, so I can keep track of the story, attributions, etc.*
--[Quaid](/wiki/User%3AQuaid "wikilink") 00:06, 17 June 2010 (UTC)

Rebecca Fernandez wrote this article:

<http://opensource.com/business/10/5/build-authentic-valuable-online-community>

... and in the comments Jason hibbets pointed out ...

<http://opensource.com/business/10/5/build-authentic-valuable-online-community#comment-1731>

... that PJ had picked up Rebecca's request for help in filling out the
missing blanks in TOSW, and so we got ..

<http://www.groklaw.net/article.php?story=20100614034659206>

(Quoted opening below.)

I read all that (great stuff), glanced at the comments (wow, lots),
wrote up an appeal to PJ to relicense, and along the way ... I read
something that reminded me to read the other comments first and not
start my own thread if it belongs under another. So I skimmed all the
comments, leaving out all the side discussions about "Google
{should,shouldn't}" and such, and did find one where PJ explained why
she would NOT be using a different license than the NC/ND for Grokla
works.

So I wrote this:

[http://www.groklaw.net/comment.php?mode=display&sid=20100614034659206&title=Thanks%2C%20inclusion%20in%20The%20Open%20Source%20Way&type=articleℴ](http://www.groklaw.net/comment.php?mode=display&sid=20100614034659206&title=Thanks%2C%20inclusion%20in%20The%20Open%20Source%20Way&type=articleℴ)=&hideanonymous=0&pid=0\#c856388
