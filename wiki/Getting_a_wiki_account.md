---
title: Getting a wiki account
permalink: wiki/Getting_a_wiki_account/
layout: wiki
tags:
 - Help
 - Documentation
---

This is the process for getting access to edit this wiki.

Email the lead writer, [Karsten Wade](/wiki/User%3AQuaid "wikilink").

Introduce yourself, why you want to edit the wiki, and anything else
relevant.

If you have a small edit for the wiki and do not want a full edit
account, consider using the "Talk:" pages found by clicking on the
"discussion" tab for the page you wish to edit.
