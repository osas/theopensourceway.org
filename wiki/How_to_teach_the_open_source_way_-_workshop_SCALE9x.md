---
title: How to teach the open source way - workshop SCALE9x
permalink: wiki/How_to_teach_the_open_source_way_-_workshop_SCALE9x/
layout: wiki
tags:
 - Events
 - Teaching the open source way
---

This is a workshop to teach others how to teach the open source way in
any circumstance. The event is being held for a general audience at
[SCALE 9x](http://www.socallinuxexpo.org/scale9x/).

*Status: Working on scheduling and location details.*

**Workshop coordinators:**

-   [Karsten 'quaid' Wade](/wiki/User%3AQuaid "wikilink")

"Teaching the open source way" workshop details
-----------------------------------------------

Three or more hours.

| Time        | Activity                                                                                                                   |
|-------------|----------------------------------------------------------------------------------------------------------------------------|
| 0:00 - 0:15 | Introduction to the handbook "The Open Source Way", how and why it is beyond *just software*.                              |
| 0:15 - 0:45 | Open discussion of challenges around getting others to see how the open source way can be adapted to their problem domain. |
| 0:45 - 2:00 | Exercises (TBD)                                                                                                            |

"The Open Source Way" info session details
------------------------------------------

One hour session.

| Time        | Activity                                                                         |
|-------------|----------------------------------------------------------------------------------|
| 0:00 - 0:20 | Introduction to The Open Source Way, how and why it is beyond *just software*.   |
| 0:20 - 0:55 | How to teach others about the handbook and methodology, Q&A, freeform discussion |

Schedule
--------

### Friday

-   *4 pm to 4:30 pm* :: **The Open Source Way info session** presented
    to general audience at Fedora Activity Day (FAD).
    -   *Location* :: Fedora Activity Day (FAD) room.

### Saturday

-   *9 am to 11 am* :: **How to teach the open source way workshop, part
    one** Introduction to The Open Source Way, how-to is hands-on
    participation including exercises.
    -   *Location* :: *TBD*
-   *1 pm to 5 pm* :: **How to teach the open source way workshop, part
    two** How-to is hands-on participation including exercises.
    -   *Location* :: *TBD*

### Sunday

-   *4 pm to 5 pm* :: **The Open Source Way info session** presented to
    general audience (encore opportunity if called for.)
    -   *Location* :: Open Source Software in Education (OSSIE) room.

Where
-----

-   Friday in Fedora Activity Day (FAD) room.
-   Saturday in a TBD room.
-   Sunday in the OSSIE workshop room.

Curriculum
----------
