---
title: Example needed
permalink: wiki/Example_needed/
layout: wiki
tags:
 - How to
 - Contributing to TOSW
---

The content that contained the link to [Example
needed](/wiki/Example_needed "wikilink") could really use a good example for
the principle under discussion.

Can you think of any?

Good examples are:

-   Brief - two or three paragraphs.
-   Focused - they illustrate the single principle. (If you have a
    longer example that illustrates several principles, it might belong
    in [Great stories to tell](/wiki/Great_stories_to_tell "wikilink")
    instead.)
-   Anonymous - it's usually safer to make a story anonymous, especially
    if there is the potential for embarrassment or worse for the people
    or organizations involved. We're nice here, we don't play that way.
-   Illustrative - the principle under discussion should be the focus.
-   General interest - we want to get people to look beyond software and
    technology.
-   Heroic - if a person or organization has done something publically
    and already accepted the wider fame around it, then it is safe to
    use as an example and sometimes makes for a powerful one.

