---
title: Big task list
permalink: wiki/Big_task_list/
layout: wiki
tags:
 - Tasks
 - Contributing to TOSW
 - Project management
---

**Updated 2010-02-16**

This is one, big task list.

Content
-------

-   Incorporate
    <http://people.redhat.com/rjones/how-to-supply-code-to-open-source-projects/>
    -   Ensure license terms in place
-   Reference, draw from under CC BY SA
    <http://www.booki.cc/collaborativefutures/credits/>
-   Rename sections to something snappier and more memorable, where
    needed.
    -   Useful for the process of making each section a stand-alone
        page.

### Principles

Some sections are still just a title (an assertion) without a properly
written principle and implementation.

If you see one that you have a clear idea for, add it.

### Implementations

Other sections have a weak or missing implementation detail. This is
where we explain exactly how to apply a principle.

If you've ever worked with the principle, perhaps you have some
suggestions on how it should be implemented?

### Examples

The book is full of examples intentionally left blank. We want examples
that are beyond our experiences in free and open source software.

What can you think of that are good examples of the principles of the
open source way?

Wiki
----

-   Does the [How to create a user
    account](/wiki/How_to_create_a_user_account "wikilink") page work and make
    sense?
-   What new [Category%3AHow to](/wiki/Category%3AHow_to "wikilink") pages do
    we need?
    -   Take a look at fedoraproject.org, e.g.
        <https://fedoraproject.org/wiki/Help:Editing>
        -   Make sure to attribute in the "Summary:" field with the URL
            back to the source page.

### Topic-specific pages

MediaWiki works best when each node of information is a unique page.
These can be transcluded to make single source pages for a group of
information. In this book, each chapter has multiple sections and
sub-sections. Each section and probably each next-level sub-section need
to be converted to individual pages.

1.  Take a chapter.
2.  Create a new page for each == Section 2 == named the same as the
    section name.
    -   The new page should *not* include the page title as a separate
        section header; remove that header and let the page stand as the
        "header 1" for that page.
3.  Make certain that each new page is in relevant categories:
    -   [Category%3AThe Open Source Way
        book](/wiki/Category%3AThe_Open_Source_Way_book "wikilink")
    -   [Category%3AName of chapter it is included
        in](/wiki/Category%3AName_of_chapter_it_is_included_in "wikilink")
    -   [Category%3ABook chapter
        sections](/wiki/Category%3ABook_chapter_sections "wikilink")
4.  In the chapter page, use transclusion to pull in the sub-sections.
    -   Include appropriate categories for the chapter
        -   [Category%3AThe Open Source Way
            book](/wiki/Category%3AThe_Open_Source_Way_book "wikilink")
        -   [Category%3AName of
            chapter](/wiki/Category%3AName_of_chapter "wikilink")
5.  Sections may be transcluded in to other sections, and then
    aggregated in to chapters, but the section markup needs to be
    manually organized. For example:
    1.  [Chapter 1](/wiki/Chapter_1 "wikilink") pulls in (transcludes)
        [Section A](/wiki/Section_A "wikilink")
    2.  [Section A](/wiki/Section_A "wikilink") transcludes a number of
        subsections, such as [Section Q](/wiki/Section_Q "wikilink")
    3.  Make sure that the == Sections == are properly set to give the
        desire nesting. You can tell when things are off by checking the
        table of contents on the page doing the top-level transclusion.

DocBook XML
-----------

-   Upload stand-alone git repo to fedorahosted.org. Tarball of the repo
    from [User%3AQuaid](/wiki/User%3AQuaid "wikilink")'s system.
    -   <http://quaid.fedorapeople.org/TOSW/The_Open_Source_Way-git_repo.tgz>
    -   Ask for permission on <https://fedorahosted.org/tosw>
-   Configure access on <https://fedorahosted.org/tosw>
-   Configure Tract on <https://fedorahosted.org/tosw>

Sysadmin
--------
