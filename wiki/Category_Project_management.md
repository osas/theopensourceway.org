---
title: Category:Project management
permalink: wiki/Category:Project_management/
layout: tagpage
tag: Project management
---

This top-level category is for pages and sub-categories related to
project management for the handbook project. This includes any pages
about the project management of these work areas:

-   Writing projects.
-   Editing projects.
-   Translation of content.
-   Wiki maintenance.
-   Website maintenance (surface).
-   Systems administration (under the surface).
-   Marketing projects.
-   And so forth ...

