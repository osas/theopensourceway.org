---
title: The Open Source Way
permalink: wiki/
redirect_to: /wiki/The_Open_Source_Way:Creating_and_nurturing_communities_of_contributors/
---

You should automatically be redirected to [The Open Source Way](/wiki/The_Open_Source_Way:Creating_and_nurturing_communities_of_contributors/)
