---
title: Contribution policy
permalink: wiki/Contribution_policy/
layout: wiki
tags:
 - Legal
---

Any contribution you make to TheOpenSourceWay.org is governed by this
contribution policy. You accept this as part of the act of making a
contribution. A contribution includes but is not restricted to code,
documentation, other written content including wiki edits, design,
translations, testing, and bug reporting.

In making this contribution, you are asserting that you have the right
to make the contribution. For example, the contribution is entirely your
own work and either (a) your employer or some other entity does not have
rights in such work, or (b) such entity has authorized you to make the
contribution on its behalf.

You irrevocably agree to release your contribution under the license
that already governs the document you are editing or the source file you
are patching. If the contribution is new non-software content, it needs
to be under the Creative Commons Attribution-Share Alike (BY SA) 3.0
Unported license to be compatible with the rest of the content of the
website and book. If the contribution is a new software source file, you
can put it under whatever license you want, understanding that the
TheOpenSourceWay.org project may accept or reject the license, or ask
for a particular license in accepting the contribution.

Currently the project has code and content under these licenses:

-   [Creative Commons Attribution-Share Alike (BY SA) 3.0
    Unported](http://creativecommons.org/licenses/by-sa/3.0/) for
    content and media.
-   [GNU General Public License (GPL)
    v.3](http://www.gnu.org/licenses/gpl-3.0.html) for scripts running
    in theopensourceway.org server infrastructure.

As a contributor, you agree to be attributed for your work in a manner
consistent with the infrastructure of the TheOpenSourceWay.org project,
such as attribution in a commit log or wiki page history, and any
attributions in derivative works.

*(Short URL: <http://bit.ly/TOSWContribPolicy> )*
