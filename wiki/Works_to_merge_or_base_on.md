---
title: Works to merge or base on
permalink: wiki/Works_to_merge_or_base_on/
layout: wiki
---

Open by Rule (OBR)
------------------

A benchmark for checking how open the governance of an open source
project is

<http://webmink.com/essays/open-by-rule/>
