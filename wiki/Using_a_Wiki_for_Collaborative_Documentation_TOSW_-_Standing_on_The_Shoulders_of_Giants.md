---
title: Using a Wiki for Collaborative Documentation TOSW - Standing on The Shoulders of Giants
permalink: wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Standing_on_The_Shoulders_of_Giants/
layout: wiki
tags:
 - Using a Wiki for Collaborative Documentation TOSW
---

*The purpose of this chapter is to show how we just reuse methodologies
and practices from the MediaWiki community, then note the differences
where we do stuff ... differently.*

History of MediaWiki
--------------------

### Why we use MediaWiki procedures and processes

### How to use MediaWiki procedures and processes

1.  step 1
2.  step 2
3.  ...
4.  PROFIT!

Processes from the Fedora Project
---------------------------------

*Link out to Fedora wiki help pages*

Processes we made up ourselves
------------------------------
