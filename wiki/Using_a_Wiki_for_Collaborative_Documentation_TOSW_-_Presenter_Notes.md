---
title: Using a Wiki for Collaborative Documentation TOSW - Presenter Notes
permalink: wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Presenter_Notes/
layout: wiki
tags:
 - Using a Wiki for Collaborative Documentation TOSW
---

These are the notes being used for a presentation on [Using a Wiki for
Collaborative Documentation
TOSW](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW "wikilink").

checklist
---------

1.  dial in to phone num. at 11:45
2.  Have demos, documents ready to go on machine I'm presenting on
    -   gwibber and noisy stuff off
    -   windows minimized save browser and emacs
    -   Stand-alone browser ready with:
        -   tosw.org front page
        -   sandbox page

timeline / outline
------------------

-     
    00 - :03 -- Introductions

-     
    03 - :05 -- Focus of talk, goals

-     
    05 - :10 -- Wiki collaboration methodology

    -   generic & specific
    -     
        10 - :25 -- Demonstration of methodology

    -     
        25 - :35 -- Wrap-up

Focus of talk, goals
--------------------

This talk presents a collaboration methodology that uses a wiki and a
common set of practices grounded in the open source way. The goal of the
collaboration is community documentation, which is content created by a
community for its own purposes/needs.

For example, I was at OSBridge last week, and each session page has a
wiki link. Audiences members are encouraged to use it for taking session
notes, and multiple people can edit parts of the notes simultaneously.
At certain conferences I've seen a very high degree of session note
taking, so the conference is literally documenting itself as it
proceeds. I can refer back to a discussion or decision that was reached
in one of those sessions, using the realtime log/minutes created by the
audience.

In Fedora, we use this for the release notes. In TOS, we use it for a
textbook. We use it for TOSW, which we are going to use today.

This content, when ready, gets converted to DocBook XML and configured
to be processed by Publican, which is a tool made and used by Red Hat
Content Services on all our product documentation. That gives us HTML,
PDF, epub, etc. XML is a great format for translation and fits directly
in with established l10n practices, such as Transifex.net.

By the end of this talk, you should know:

-   How to use a wiki for collaborative documentation.
-   How to create a set of interlinked pages, such as a book or guide.
-   How to edit MediaWiki pages.
-   Why and where you might want to use this methodology.
-   Have an invitation to help edit/write The Open Source Way.

Wiki collaboration methodology
------------------------------

We are building on what Wikipedia teaches us. So, we point to their
how-to and why information.

-   MediaWiki help pages provide syntax information and best practices
    on page authoring:
    -   <https://www.theopensourceway.org/wiki/Help:Editing> points to
        <http://fedoraproject.org/wiki/Help:Editing>
    -   <http://fedoraproject.org/wiki/Help:Wiki_rules_and_etiquette>
    -   <http://www.mediawiki.org/wiki/Help:Contents>
-   Fedora Project grew up special rules
    -   to handle our unique situation
    -   to make it easier to convert wiki pages to DocBook XML so it can
        be processed by the same tools that build RHEL docs.
-   Examples
    -   Fedora release notes
    -   TOS textbook
    -   TOSW

Going to a printable form
-------------------------

-   For TOSW, Fedora Project, Teaching Open Source -- we use a tool that
    lets us automagically convert the wiki pages to XML files. It does
    about 85% of the structural work to get documents that work with
    Publican. The content may need some or a lot of inline editing,
    depending on how much markup occurs in the paragraphs. (More so for
    more technical content, usually, or for heavy references to user
    interfaces, etc.)
-   [Converting\_to\_DocBook\_XML](/wiki/Converting_to_DocBook_XML "wikilink")

