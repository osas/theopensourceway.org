---
title: Business the open source way
permalink: wiki/Business_the_open_source_way/
layout: wiki
tags:
 - The Open Source Way book
---

*This chapter is an overview of how a business can use the open source
way to succeed in growing revenue and brand. As in other chapters,
principles are explained, implementation is described, and examples are
provided.*

Resources: <http://darkmattermatters.com>

Making money from Free
----------------------

From Glyn Moody,
<http://opendotdotdot.blogspot.com/2009/09/someone-has-man-with-red-flag-moment.html>,
a reminder:

> Being able to give away copies of your work freely is an opportunity,
> not a threat: it's called marketing, and it's cost has just gone down
> to nothing.

References
----------

-   Bob Sutor - "Starting an open source business: preliminary
    thoughts" - <http://www.sutor.com/newsite/blog-open/?p=4599>

