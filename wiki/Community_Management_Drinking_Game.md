---
title: Community Management Drinking Game
permalink: wiki/Community_Management_Drinking_Game/
layout: wiki
---

The new Community Management Drinking Game is a set of when-to-drink
rules triggered by the sort of shenanigans that come up in online
mailing lists, forums, IRC channels, and in person events throughout
open source communities. The game is growing to include tropes and
patterns found in documentation and other content sources.

-   New user doesn't read FAQ -- *pour a drink but don't drink it ...
    yet*
-   New user asks question on FAQ after being pointed at the FAQ -- *now
    drink it.*
-   "WWIC - why wasn't I consulted?" -- *1 drink*
-   A useless "me too" or "+1" is posted -- *1 drink*
-   A new user is told to RTFM -- *1 drink*
-   "Your employer is a corporate overlord secretly conspiring to
    control or destroy the project" -- *1 drink*
-   Godwin's Law occurs -- *guzzle from the bottle*
-   Someone invokes Godwin's Law to stop the discussion, and it works --
    *go in to recovery*

