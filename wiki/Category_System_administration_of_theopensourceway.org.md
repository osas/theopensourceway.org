---
title: Category:System administration of theopensourceway.org
permalink: wiki/Category:System_administration_of_theopensourceway.org/
layout: tagpage
tag: System administration of theopensourceway.org
---

This is a category collecting pages related to administrating the
systems that run theopensourceway.org.

For more information, refer to the [System administration
team](/wiki/System_administration_team "wikilink") page.
