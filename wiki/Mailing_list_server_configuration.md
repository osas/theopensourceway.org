---
title: Mailing list server configuration
permalink: wiki/Mailing_list_server_configuration/
layout: wiki
tags:
 - System administration of theopensourceway.org
---

Install notes
-------------

These are a collection of notes taken during installation and
configuration.

Configure firewall to allow port 25.

    iptables -A INPUT -p tcp --dport 25 -j ACCEPT
    service iptables save

Install mailman - 'yum install mailman'

Add/change these bits in /etc/postfix/main.cf:

    myorigin = $mydomain
    inet_interfaces = localhost, $myhostname, $mydomain, lists.theopensourceway.org
    mydestination = $myhostname, localhost.$mydomain, localhost, lists.theopensourceway.org, $mydomain

Look at /etc/httpd/conf.d/mailman.conf.

Set /mailman to redirect to listinfo page:

    # Uncomment the following line, to redirect queries to /mailman to the
    # listinfo page (recommended).

    RedirectMatch ^/mailman[/]*$ http://lists.theopensourceway.org/mailman/listinfo

Edit /etc/mailman/mm\_cfg.py

    DEFAULT_URL_HOST   = "lists.theopensourceway.org"
    DEFAULT_EMAIL_HOST = "theopensourceway.org"

Add new virtual host to
/etc/httpd/conf.d/lists.theopensourceway.org.conf

    <VirtualHost *:80>
        ServerAdmin webmaster@lists.theopensourceway.org
        DocumentRoot /var/www/html/
        ServerName lists.theopensourceway.org
        ErrorLog logs/lists.theopensourceway.org-error_log
        CustomLog logs/lists.theopensourceway.org-access_log common
    </VirtualHost>

Restart Apache.

chkconfig {postfix,mailman} on

Run /usr/lib/mailman/bin/mmsitepass to set mailman password password:
(refer to /root/passwords)

Run /usr/lib/mailman/bin/newlist to create a new list

mailman - Special, required Mailman list password: (refer to
/root/mailing-list-passwords)

metrics-wg - Metrics working group password: (refer to
/root/mailing-list-passwords)

    To finish creating your mailing list, you must edit your /etc/aliases (or
    equivalent) file by adding the following lines, and possibly running the
    `newaliases' program:

    ## mailman mailing list
    mailman:              "|/usr/lib/mailman/mail/mailman post mailman"
    mailman-admin:        "|/usr/lib/mailman/mail/mailman admin mailman"
    mailman-bounces:      "|/usr/lib/mailman/mail/mailman bounces mailman"
    mailman-confirm:      "|/usr/lib/mailman/mail/mailman confirm mailman"
    mailman-join:         "|/usr/lib/mailman/mail/mailman join mailman"
    mailman-leave:        "|/usr/lib/mailman/mail/mailman leave mailman"
    mailman-owner:        "|/usr/lib/mailman/mail/mailman owner mailman"
    mailman-request:      "|/usr/lib/mailman/mail/mailman request mailman"
    mailman-subscribe:    "|/usr/lib/mailman/mail/mailman subscribe mailman"
    mailman-unsubscribe:  "|/usr/lib/mailman/mail/mailman unsubscribe mailman"

    ## metrics-wg mailing list
    metrics-wg:              "|/usr/lib/mailman/mail/mailman post metrics-wg"
    metrics-wg-admin:        "|/usr/lib/mailman/mail/mailman admin metrics-wg"
    metrics-wg-bounces:      "|/usr/lib/mailman/mail/mailman bounces metrics-wg"
    metrics-wg-confirm:      "|/usr/lib/mailman/mail/mailman confirm metrics-wg"
    metrics-wg-join:         "|/usr/lib/mailman/mail/mailman join metrics-wg"
    metrics-wg-leave:        "|/usr/lib/mailman/mail/mailman leave metrics-wg"
    metrics-wg-owner:        "|/usr/lib/mailman/mail/mailman owner metrics-wg"
    metrics-wg-request:      "|/usr/lib/mailman/mail/mailman request metrics-wg"
    metrics-wg-subscribe:    "|/usr/lib/mailman/mail/mailman subscribe metrics-wg"
    metrics-wg-unsubscribe:  "|/usr/lib/mailman/mail/mailman unsubscribe metrics-wg"

### Resources used

<http://www.iredmail.org/forum/topic1582-howto-configure-mailman-on-centosrhel-5.html>

<http://linux.indhran.info/2008/12/how-to-setup-mailman-in-redhat.html>

/usr/share/doc/mailman/INSTALL.REDHAT
