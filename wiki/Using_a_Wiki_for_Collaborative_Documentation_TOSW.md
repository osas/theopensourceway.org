---
title: Using a Wiki for Collaborative Documentation TOSW
permalink: wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW/
layout: wiki
tags:
 - Documentation
 - How to
 - Using a Wiki for Collaborative Documentation TOSW
---

These pages and subsequent chapters were created during a live
presentation on how to use MediaWiki for collaborative documentation.

More writing may be done here otherwise, and some saved for future
presentations or classroom sessions.

Chapters
--------

-   [Using a Wiki for Collaborative Documentation TOSW -
    Introduction](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Introduction "wikilink")
-   [Using a Wiki for Collaborative Documentation TOSW - Standing on The
    Shoulders of
    Giants](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_Standing_on_The_Shoulders_of_Giants "wikilink")
-   [Using a Wiki for Collaborative Documentation TOSW - The
    Process](/wiki/Using_a_Wiki_for_Collaborative_Documentation_TOSW_-_The_Process "wikilink")

