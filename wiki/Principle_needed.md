---
title: Principle needed
permalink: wiki/Principle_needed/
layout: wiki
tags:
 - How to
 - Contributing to TOSW
---

The content that contained the link to [Principle
needed](/wiki/Principle_needed "wikilink") could really use a good write-up
explaining the principle.

Can you think of one?

A good write-up for a principle is:

-   Brief - one or two paragraphs.
-   Focused - be sure it is a single principle and not one or more
    principles in a collection. If it is the latter, perhaps it is
    really belongs in [Great stories to
    tell](/wiki/Great_stories_to_tell "wikilink")?
-   General - de-couple the principle from the domain of origin. Many of
    the principles come from the open source software movement, so must
    be explained in a way that de-couples it from software and
    technology to allow the audience to understand it for their own
    domains. For this reason, the principle should be anonymous rather
    than about a specific domain or organization.

