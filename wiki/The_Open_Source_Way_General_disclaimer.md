---
title: The Open Source Way:General disclaimer
permalink: wiki/The_Open_Source_Way:General_disclaimer/
layout: wiki
tags:
 - Legal
---

This is a community-maintained site. Red Hat, Inc. is not responsible
for its content.

All contributors to this project agree to the [Contribution
policy](/wiki/Contribution_policy "wikilink").

Red Hat, Red Hat Enterprise Linux, the Shadowman logo, JBoss,
MetaMatrix, Fedora, the Infinity Logo, and RHCE are trademarks of Red
Hat, Inc., registered in the United States and other countries.

All other trademarks throughout this wiki are the property of their
respective owners.

The wiki and related content is copyright (c) 2009 - 2010 by Red Hat,
Inc. and others.

The dandelion logo used beginning 12-Feb-2010 is from an image at
<http://www.flickr.com/photos/leonwp/14125152/> that is under the [CC BY
SA 2.0](http://creativecommons.org/licenses/by-sa/2.0/deed.en) license.
The updated logo and favicon are copyright 2010 by Red Hat, Inc. and are
released under the sitewide [CC BY SA 3.0
Unported](http://creativecommons.org/licenses/by-sa/3.0/) license.
