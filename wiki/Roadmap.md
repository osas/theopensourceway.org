---
title: Roadmap
permalink: wiki/Roadmap/
layout: wiki
tags:
 - Roadmap
 - Contributing to TOSW
---

Background
----------

This roadmap is currently feature based, rather than time based.

As progress is made, we may want to change to a time based schedule to
take advantage of the rapid progress and attention that gets.

In general, the goal is to have progress on all these roadmap details by
January 2011.

Details
-------

1.  Reorganize wiki and chapters to reflect new [Table of
    Contents](https://fedorahosted.org/pipermail/tosw/2010-August/000011.html).
    -   Includes making at last all first-level nested sections into
        stand-alone wiki pages for easier reference, then stitch
        together in to chapters using transclusion.
2.  Reorganize XML to reflect new organization on the wiki.
3.  Generate PO file from the new organization.
4.  Organize all open tasks onto Trac instance.
5.  Reorganize roadmap to be time-based to help set writing community
    rhythm.
6.  Seek contributors and participants who can help clean up the current
    content.
7.  Find new example stories to fill the gaps.
8.  Rewrite content to be easier to translate.
9.  Recruit domain experts to help with "X the open source way"
    chapters.
10. Seek existing material to kickstart "X the open source way"
    chapters.
11. Finish working on the principles chapters beyond reorganization:
    -   Make sure they reflect all the known principles.
    -   Make sure they have shorter natural URLs, along with relevant
        short URLs from e.g. bit.ly

