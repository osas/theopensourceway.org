---
title: Software the open source way
permalink: wiki/Software_the_open_source_way/
layout: wiki
tags:
 - The Open Source Way book
 - Draft
---

Exemplar projects
-----------------

(Tiemann, March 2009): I would suggest that we hold up specific projects
as exemplars in their respective domains (and also which projects are
exemplary in the negative).

In the positive I would say:

-   Blender: user-driven innovation (via the open movies that get
    artists, animators, and hackers all talking)
-   GRASS and R: loosely coupled
-   glibc: a cathedral that works (rigorous, unimpeachable adherence to
    standards frustrates many, but helps the most succeed)

Obvious the list can grow as well, but if various stakeholders were to
try to rank their project's attributes against those defined by the
cookbook, everybody would learn something.
