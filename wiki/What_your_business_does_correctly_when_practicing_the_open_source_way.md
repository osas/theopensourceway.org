---
title: What your business does correctly when practicing the open source way
permalink: wiki/What_your_business_does_correctly_when_practicing_the_open_source_way/
redirect_to: /wiki/What_your_organization_does_correctly_when_practicing_the_open_source_way/
---

You should automatically be redirected to [What your organization does correctly when practicing the open source way](/wiki/What_your_organization_does_correctly_when_practicing_the_open_source_way/)
