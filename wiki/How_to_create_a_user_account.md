---
title: How to create a user account
permalink: wiki/How_to_create_a_user_account/
layout: wiki
tags:
 - How to
 - Documentation
 - Contributing to TOSW
---

If you need an account, contact one of the [existing account
holders](/wiki/Special:ListUsers "wikilink"), who are fellow participants and
contributors.

These instructions are for a person [who already has an
account](/wiki/Special:ListUsers "wikilink") and wants to grant an account to
a new participant. **Use these powers wisely**:

1.  Log in with your user account.
2.  Got to the login page <Special:UserLogin>
3.  Use the link labeled [Create an
    account](https://www.theopensourceway.org/w/index.php?title=Special:UserLogin&type=signup),
    or click this link here
4.  Fill out the account information:
    -   Username
    -   Password - *leave blank*
        -   A randomly generated password is sent to the user's email
            address
    -   E-mail
    -   Real name
5.  Uncheck "Remember my login on this computer".
6.  Click "by e-mail" to send the new account information directly to
    the user.

If your browser offers to save the password for this new user, choose no
or "Not now".
