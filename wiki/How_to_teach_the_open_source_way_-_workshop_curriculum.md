---
title: How to teach the open source way - workshop curriculum
permalink: wiki/How_to_teach_the_open_source_way_-_workshop_curriculum/
layout: wiki
tags:
 - Curriculum
---

Goals of the workshop are:

-   Engage with an audience as students and teachers - learn from each
    other by contributing, e.g. to TOSW or TOS textbook.
-   Audience learning objectives:
    -   Is comfortable with giving increasing complexities of
        explanations of the open source way that build on each other
        and: 15-second, 30-second, 1-minute, 2-minute, 3-minute,
        5-minute, 10-minute, 20-minute, 30-minute.
    -   Is able to use knowledge of the open source way and associated
        handbook to affect a project or program in a software domain to
        move in a more open, collaborative, radically transparent
        direction.
    -   Is able to use knowledge of the open source way and associated
        handbook to affect a project or program in a non-software domain
        to move in a more open, collaborative, radically transparent
        direction.
    -   Can teach others at least a portion of what they learned in the
        workshop, thereby spreading the knowledge the open source way.
-   Audience contributing objectives:
    -   Add three examples to The Open Source Way that are not from the
        FLOSS world.
    -   Work with one or more other audience memnbers to help complete
        two or more implementation sections.
    -   Advance or begin a chapter on implementation, "Foo the open
        source way".

