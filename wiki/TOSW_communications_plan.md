---
title: TOSW communications plan
permalink: wiki/TOSW_communications_plan/
layout: wiki
tags:
 - TOSW project management
 - TOSW communications
 - TOSW
---

Rhythm
------

This is the rhythm for communication, and starts at the beginning of
March 2011 with a basic plan to accomplish one specific thing for each
time period passage.

### Daily

-   [TOSW daily homily](/wiki/TOSW_daily_homily "wikilink") - daily via
    identi.ca, twitter, blog

### Weekly

-   TOSW edit for opensource.com queue

### Bi-weekly

-   [Opensource.com articles](/wiki/Opensource.com_articles "wikilink")

### Monthly

-   TOSWCast - Audio production (oggcast/podcast) focused on current
    events around the world as viewed through the lens of the open
    source way. Jan Wildeboer, Karsten Wade

### Quarterly

-   ???

Comms channels details
----------------------

### TOSW homily

Something plucked from the book as a thing to ponder, or compared to
some current event (in or out of technology.)

Could use to build up 3 weeks worth (15 total) at [TOSW daily
homily](/wiki/TOSW_daily_homily "wikilink"), where past ones can be archived.
Using short URLs are key here, so keep creating and building up the
supply as referenced on tosw.org.

Homily is a codeword for now; it has a religious connotation that we may
not want to carry forward.

For now, these can all start at identi.ca/quaid (which auto-posts to
twitter.com/quaid), but we might want to have @opensourceway do them in
the future?

### opensource.com article

Karsten needs to drive this one, as it is somewhat time intensive and
right up his proverbial alley.

### TOSWCast

-   Can we use Dan Lynch (THiL, Linux Outlaws, FaiFcast) to do post
    production for us?
-   Karsten and Jan, at the least. There have been some other
    (external?) interest in helping, who was that? Robyn? Amber?

