---
title: The Open Source Way:Creating and nurturing communities of contributors
permalink: wiki/The_Open_Source_Way:Creating_and_nurturing_communities_of_contributors/
layout: wiki
tags:
 - The Open Source Way book
---

**The Open Source Way is a book shows how to (and how *not* to) engage
with the community members on projects of any type by practicing the
open source way.**

**This handbook distills years of knowledge our community members have
gained while running open source projects.**

![](/images/Barnraising_TOSW.jpg "Barnraising_TOSW.jpg")

For stories and discussions about the open source way, visit our
sister-site [opensource.com](http://opensource.com).

Book format
-----------

This content is converted regularly to other formats for web and
printing publication:

-   [Multi-page HTML](http://www.theopensourceway.org/book)
-   [Single-page HTML](http://www.theopensourceway.org/book/single-page)
-   [PDF](http://www.theopensourceway.org/book/The_Open_Source_Way.pdf)
-   [Epub](http://www.theopensourceway.org/book/The_Open_Source_Way.epub)

Wiki upstream content -- the raw content workshop
-------------------------------------------------

This is [The Open Source Way](/wiki/The_Open_Source_Way "wikilink") *wiki
upstream*. It is ready for you to read and add to.

1.  [The Open Source Way: Creating and nurturing communities of
    contributors](/wiki/The_Open_Source_Way:_Creating_and_nurturing_communities_of_contributors "wikilink")
    -- Main wiki page
2.  [Introduction](/wiki/Introduction "wikilink") -- What this book is, what
    it is not, essential terminology (definitions and pointers)
3.  [Communities of practice](/wiki/Communities_of_practice "wikilink") -- The
    science behind the open source way
4.  [How to loosely organize a
    community](/wiki/How_to_loosely_organize_a_community "wikilink") --
    Structure of a sustainable community
5.  [Stuff everyone knows and forgets
    anyway](/wiki/Stuff_everyone_knows_and_forgets_anyway "wikilink") --
    Common knowledge, common mistakes, common recoveries
6.  [What your organization does correctly when practicing the open
    source
    way](/wiki/What_your_organization_does_correctly_when_practicing_the_open_source_way "wikilink")
    -- Distilled "Right Way"
7.  [What your organization does wrong when practicing the open source
    way](/wiki/What_your_organization_does_wrong_when_practicing_the_open_source_way "wikilink")
    -- Distilled "Wrong Way"
8.  [Business the open source
    way](/wiki/Business_the_open_source_way "wikilink") -- Applying the open
    source way to business
9.  [The Openness Index](/wiki/The_Openness_Index "wikilink") -- A maturity
    model for assesing alignment with the Open Source Way.
10. [Who else to watch](/wiki/Who_else_to_watch "wikilink") -- People in the
    wider world who write, talk, and lead community-oriented discussions
    and actions
11. [Books you should read](/wiki/Books_you_should_read "wikilink") -- Online
    and offline resources
12. [Great stories to tell](/wiki/Great_stories_to_tell "wikilink") -- Stories
    that illustrate the open source way
13. [Community Architecture
    dashboard](/wiki/Community_Architecture_dashboard "wikilink") -- Overview
    of this effort with pointers
14. [Data and references](/wiki/Data_and_references "wikilink") -- Data and
    references that support the book
15. [How to tell if a FLOSS project is doomed to
    FAIL](/wiki/How_to_tell_if_a_FLOSS_project_is_doomed_to_FAIL "wikilink") -
    A system for evaluating the potential of an open source project
16. [Appendix](/wiki/Appendix "wikilink") -- What fits not elsewhere but only
    here

