---
title: Organizing a community - checklist
permalink: wiki/Organizing_a_community_-_checklist/
layout: wiki
tags:
 - Checklist
---

This is a checklist to follow for starting a new community, merging two
or more communities, or bringing an existing project and internal
community out in to the wider open air. Giving some thought to your
[community's technology
infrastructure](http://cpsquare.org/wiki/Other_tools_directories)
(specific tools for specific purposes) needs to happen before and all
along the launch of your project.

1.  Mission and project vision
2.  Initial governance.
3.  Contribution policy.
    1.  If there is copyright works being contributed to a commons, then
        a minimum contribution policy should be maintained. Some
        projects also use a contributor license agreement (CLA).
4.  External main project mailing list.
    -   You may choose to hold-off on a general/user discussion list if
        it's not appropriate for the project, or until they get too
        annoying in the main project coordination list and need a list
        of their own.
5.  Source control:
    1.  Code.
    2.  Content (could be wiki), includes artwork, audio, and so forth.
6.  Issue tracker is a general tool or method for the community to keep
    track of important issues (projects, problems, tasks) in a central
    way.
7.  Wiki for community, collaborative documentation.
8.  Weekly IRC meeting time.
9.  Team planet/blog feed.
10. Open roadmap for the project on the wiki.
11. Simple open marketing plan, posted on project wiki, talked about on
    main mailing list.
    1.  Conferences to submit talks to.
    2.  Events to attend
    3.  People to talk to now that there is something to talk about.
    4.  Local events (user groups, meetups) to attend or organize.
    5.  Articles for magazines or websites.
    6.  Hosting online seminars.
12. Expose interesting and easier tasks.
    1.  Leave smaller work undone and ask for help on such tasks.
    2.  Look for ways to encourage [peripheral
        participation](/wiki/Communities_of_practice#Invite_different_levels_of_participation "wikilink").
13. Volunteer mentors wiki page.
14. How to participate and contribute page.
15. Community information page:
    1.  Communication methods listed (mailing lists, IRC channels, etc.)
    2.  Events attending.
    3.  Meetups happening.
    4.  User group events.
16. Participant and contributor improvements and needs page - wish list
    and roadmap for how things can/should improve for contributors and
    participants, over time.

