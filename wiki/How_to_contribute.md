---
title: How to contribute
permalink: wiki/How_to_contribute/
layout: wiki
tags:
 - Contributing to TOSW
 - How to
---

*[`Let`` ``your`` ``audience`` ``do`` ``your`` ``PR.`` ``They're`` ``pleasantly`` ``biased`` ``and`` ``decidedly`` ``not`` ``you.`](https://twitter.com/rands/statuses/2033668001)*

A bit of wisdom from [Rands](http://www.randsinrepose.com/) that applies
to communities and contributions: they are pleasantly biased and
decidedly not you.

You are encouraged (HIGHLY) to contribute your thoughts, ideas, writing,
expertise, critique, and just about anything.

Help improve this telling of how to know and implement the open source
way.

A specific **goal** of this book is *to have a community write about
community*.

This wiki is the *upstream* for the book and is where all of the
collaboration on writing takes place. We can then form wiki-specific
processes over time within the authoring community. That process is
written up in [Contributing to The Open Source
Way](/wiki/Contributing_to_The_Open_Source_Way "wikilink").

A second step is the conversion of the content to DocBook XML for
publication. That process is written up in [Converting to DocBook
XML](/wiki/Converting_to_DocBook_XML "wikilink").

The DocBook XML source is kept in a `git` repository at
<http://fedorahosted.org/tosw> . Details on getting accessing, using,
and contributing to that repository are in [Contributing to The Open
Source Way](/wiki/Contributing_to_The_Open_Source_Way "wikilink") and [Using
git repository](/wiki/Using_git_repository "wikilink").

Contribution areas
------------------

-   Wiki - any existing user can create a new user account for someone
    else
    1.  Read and understand the [Contribution
        policy](/wiki/Contribution_policy "wikilink").
        -   If you have any questions about the policy, contact the
            contributors via the mailing list; read
            [Contacts](/wiki/Contacts "wikilink") for ore information.
    2.  Contact an existing user to sponsor you on the wiki.
        -   Try searching the [User%3A](/wiki/User%3A "wikilink") pages.
    3.  Have that user follow the instructions on the page [How to
        create a user account](/wiki/How_to_create_a_user_account "wikilink").
    4.  Read [Contributing to The Open Source
        Way](/wiki/Contributing_to_The_Open_Source_Way "wikilink").
-   DocBook XML
    -   Start by joining and talking on the [mailing
        list](https://fedorahosted.org/mailman/listinfo/tosw).
    -   You need a [Fedora account
        (FAS)](https://admin.fedoraproject.org/accounts).
    -   Read [Contributing to The Open Source
        Way](/wiki/Contributing_to_The_Open_Source_Way "wikilink") and
        [Converting to DocBook
        XML](/wiki/Converting_to_DocBook_XML "wikilink").
    -   Read [Using git repository](/wiki/Using_git_repository "wikilink") for
        detailed information.
    -   To get commit access, start by talking on list and submitting
        patches.
-   [Mailing list](https://fedorahosted.org/mailman/listinfo/tosw) is
    self-service joining.

