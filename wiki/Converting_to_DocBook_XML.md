---
title: Converting to DocBook XML
permalink: wiki/Converting_to_DocBook_XML/
layout: wiki
tags:
 - The Open Source Way book
 - Contributing to TOSW
 - How to
---

This is the procedure we follow when converting content from the wiki in
to the DocBook XML, Publican-based guide in git.

Initial conversion
------------------

The initial conversion was done following the process on this page:

<https://fedoraproject.org/wiki/Converting_wiki_to_DocBook_XML>

Ongoing updates
---------------

1.  Go to each chapter's wiki page.
2.  Use the 'history' tab to bring up the changelog list for the page.
3.  Select the previous conversion point and the last change as the two
    boundaries for viewing a diff of the changes.
4.  Edit the changes revealed by the diff in to the XML.
    -   Note where the XML uses a different construct in how information
        is structured; some wiki tricks (using separate sections)
        doesn't translate as well to XML.
    -   If mistakes in the wiki are noticed, commit the fix to the wiki
        at the same time, noting in both changelogs that the fix is in
        both locations.
5.  Test the build, fix any XML errors.
6.  Commit changes to git, noting the updates come from the upstream
    wiki.
7.  Build HTML (chunked and single page) and PDF; upload new versions to
    canonical location
    -   *Current canonical:* <http://theopensourceway.org/book>

