---
title: Metrics working group session at the GSoC Mentor Summit 2011
permalink: wiki/Metrics_working_group_session_at_the_GSoC_Mentor_Summit_2011/
layout: wiki
tags:
 - Metrics working group
---

*This page is a verbatim mirror of the canonical page located at
<https://gsoc-wiki.osuosl.org/index.php/Metrics_working_group> and is
used here under the terms of the [GNU Free Documentation License
1.2](http://www.gnu.org/copyleft/fdl.html). This copy shall not be
modified in any way; all modifications are made in the canonical
upstream and re-mirrored here.*

Please use MediaWiki syntax for easy pasting in to the wiki when we are
done.

Reference:
<http://iquaid.org/2011/08/12/working-group-on-community-metrics/>

\_\_TOC\_\_ Mediawiki example:

-   Heard from the new community manager for MediaWiki
-   They have no visibility in to trending
-   What is mailing list traffic doing?
-   "How do I know who to reach out? Prevent attrition?"
-   Like to see some kind of aggregate of how people are doing
    -   Such as pulling data from bug tracker to find out where the
        hotspots of problems are.

**Could this be a first action of the working group - emergency & trauma
tracking?**

Drupal Foundation

-   Focus on running hackathons

Pentaho being used for some metrics tools.

-   Under Mozilla & Meego
-   Meego's is released, but not a drop-in
    -   Older versions, etc.
-   Mozilla is moving toward an integrated something
    -   But no source is happening yet

People to contact about metrics-wg:

-   David Eaves
-   Pedro from Pentaho
-   Paul Adams, KDE

Look at both individual contributors and companies

Tie community metrics into "product" output

-   Code
-   Content
-   Evangelism

Goals - Why have a strong community? Can you tie it into your project
goals, like:

-   Improved product
-   Happier people
-   More code

Improve the funnel of 90/9/1 lurkers/contributors/core community

<https://fedoraproject.org/wiki/Statistics>

-   An example of what little can be or has been done for tracking a
    diverse Linux distro userbase.

Participant types for the working group:

-   ACAD (Academia)
-   CORP (Corporations)
-   FOUN (Foundations)
-   FOSS (Free/open source software projects)
-   GOVT (Government)

Things to track/highlight: *(in a dashboard?)*

-   Hit-by-bus factor

Privacy
-------

-   How much do people care?
-   How do we inform people of how they are being measured?

Where next
----------

1.  Looking at Meego metrics:
    -   It's the only one out there with code we can try using right now
    -   It's usable, if you mess with it
    -   Graphs and statistics about commits, bugs, etc.
2.  Fill out
    <https://www.theopensourceway.org/wiki/Metrics_working_group>
3.  Join <http://lists.theopensourceway.org/mailman/listinfo/metrics-wg>
4.  Research sentiment analysis software
5.  Use <http://piratepad.net/tosw-metrics-wg-initial-invites>
6.  See what academia is doing

What to work on NOW:

1.  Get your own instance of what Meego has done up and running.
2.  Start collecting data now, some of the data is not kept over time.
    -   Especially for looking at lurkers
3.  Invite people to join the group

Mission
-------

**The mission of the group is to create methods for tracking community
health.**

Miscellaneous
-------------

[Etherpad](http://etherpad.osuosl.org/metrics-working-group) for group
editing of notes
