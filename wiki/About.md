---
title: About
permalink: wiki/About/
layout: wiki
tags:
 - About
---

This guide is for helping people to understand how to and how not to
engage with community over projects such as software, content,
marketing, art, infrastructure, standards, and so forth. It contains
knowledge distilled from years of Red Hat experience, which itself comes
from the many years of experience of individual upstream contributors
who have worked for Red Hat.

*[`Let`` ``your`` ``audience`` ``do`` ``your`` ``PR.`` ``They're`` ``pleasantly`` ``biased`` ``and`` ``decidedly`` ``not`` ``you.`](https://twitter.com/rands/statuses/2033668001)*

A bit of wisdom from [Rands](http://www.randsinrepose.com/) that applies
to communities and contributions - they are pleasantly biased and
decidedly not you.
