---
title: TOSW daily homily
permalink: wiki/TOSW_daily_homily/
layout: wiki
tags:
 - TOSW
 - TOSW communications
---

Something plucked from the book as a thing to ponder, or compared to
some current event (in or out of technology.)

Try to keep a backlog of 15 here for when there aren't any good current
events or time to research.

Send at least one per day, Monday through Friday.

Delivered by [@quaid](http://identi.ca/quaid/tag/tosw), under the
[\#TOSW](https://identi.ca/tag/tosw) hashtag, and forwarded to
[twitter.com/quaid](https://twitter.com/#!/quaid).

Future
------

This is a bulleted line length indicator, finishing with the last three
characters signifying the line length - 140 characters for a microblog
message.

-   Xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx140

### Communities of Practice lessons

1.  The way to find more experts is to invest in a process that
    continually creates more experts. <http://bit.ly/TOSWLPP> \#TOSW
    \#CoP
2.  Legitimate peripheral participation is the apprenticeship of
    communities of practice. <http://bit.ly/TOSWLPP> \#TOSW \#CoP
3.  Open communities have a special need for the occasional private
    conversation. <http://bit.ly/TOSWCoPPubPriv> \#TOSW \#CoP
4.  Encourage your community to express the value they get, for
    themselves & to show others. <http://bit.ly/TOSWCoPValue> \#TOSW
    \#CoP
5.  If someone else's value in your community isn't your own, leave it
    be unless it's poisonous. <http://bit.ly/TOSWCoPValue> \#TOSW \#CoP
6.  Keep your community moving & interesting, but not so rocky that
    people get uncomfortable. <http://bit.ly/TOSWFamiliarityExcitement>
    \#TOSW \#CoP
7.  Whatever your community's practice, keep the rhythm like a dance.
    <http://bit.ly/TOSWCoPRhythm> \#TOSW \#CoP
8.  A community of doers needs a pace that is neither too slow nor too
    fast. <http://bit.ly/TOSWCoPRhythm> \#TOSW \#CoP

Archive
-------

-   All communities have core \#cliques, the key is to open them for
    dialogue <http://bit.ly/TOSWOpentheClique> \#TOSW \#CoP
    *([notice](https://identi.ca/notice/68478470) - 20110328)*
-   Let your community grow, breathe, and change - design for evolution
    <http://bit.ly/TOSWDesignforEvo> \#TOSW \#CoP
    *([notice](https://identi.ca/notice/68059562) - 20110324)*
-   You Are Solving The Wrong Problem <http://bit.ly/f9xWl7> &lt;Embrace
    failure for faster innovation <http://bit.ly/TOSWFailure> \#TOSW ♺
    @glynmoody *([notice](https://identi.ca/notice/67750053) -
    20110321)*
-   No matter how mature your FOSS-supporting organization is, you may
    still break fundamental rules <http://bit.ly/TOSWBreaksCommunity>
    \#TOSW *([notice](https://identi.ca/notice/67394877) - 20110318)*
-   What makes a Community of Practice? People practicing in a specific
    domain, learning together <http://bit.ly/TOSWCoPElements> \#TOSW
    \#CoP *(https://identi.ca/notice/67311011 notice\] -- 20110317)*
-   Do you belong to a group @ work/school who work to improve a common
    practice? It's a Community of Practice <http://bit.ly/TOSWCoP>
    \#TOSW \#CoP *([notice](https://identi.ca/notice/67205239) -
    20110316)*
-   "what other bits of the business can be done in an open, sharing
    manner." <http://bit.ly/TOSWDaringOrg> <http://bit.ly/fEjL47> \#TOSW
    @bob\_sutor *([notice](https://identi.ca/notice/67010031) -
    20110314)*
    -   '...find a team of people who will also provide “in an open
        source way” the other content' <http://bit.ly/fEjL47>
        <http://bit.ly/TOSWDaringOrg>
        *([notice](https://identi.ca/notice/67009624) - 20110314)*
-   @fontana Thx again for collab on clear
    <http://bit.ly/TOSWContribPolicy> for theopensourceway.org. It's a
    good example for others. \#TOSW
    *([notice](https://identi.ca/notice/66691689), [context
    thread](http://identi.ca/conversation/65863142#notice-66691689) -
    20110310)*
-   @jspaleta \*Nicely\* said; thx to identi.ca CC BY I snarfed those
    right in to <http://bit.ly/TOSWOpenDiscussions> \#TOSW
    *([notice](http://identi.ca/notice/66515438), [context
    thread](http://identi.ca/conversation/65396445#notice-66515438) -
    20110308)*
-   One could argue \#Chatham\_House\_Rules are
    <http://bit.ly/TOSWCoPPubPriv> but I don't think so - lockup of ALL
    private comms /= \#TOSW
    *([notice](https://identi.ca/notice/66476540), [context
    thread](https://identi.ca/conversation/65396445#notice-66476540) -
    20110308)*
-   Working on <http://bit.ly/TOSWDoneWrong> in private is an example of
    <http://bit.ly/TOSWCoPPubPriv> - honesty, filter, then publish.
    \#TOSW *([notice](https://identi.ca/notice/66384300) - 20110307)*
    -   Last week added two sub-sections & an example to
        <http://bit.ly/TOSWDoneWrong> writing w/ \#RHT folks on internal
        Etherpad ... FTW! \#TOSW
        *([notice](https://identi.ca/notice/66384329) - 20110307)*

