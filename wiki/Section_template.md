---
title: Section template
permalink: wiki/Section_template/
layout: wiki
tags:
 - TOSW tools
---

*Sample section code to use when making a new section in a chapter of
[The Open Source Way](/wiki/The_Open_Source_Way "wikilink").*

    <!-- Principle needed -->
    ''[[Principle needed]]''

    <!-- Implementation needed -->
    ''[[Implementation needed]]''

    <!-- Example needed -->
    ''[[Example needed]]''
