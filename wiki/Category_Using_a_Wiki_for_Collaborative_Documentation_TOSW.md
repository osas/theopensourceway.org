---
title: Category:Using a Wiki for Collaborative Documentation TOSW
permalink: wiki/Category:Using_a_Wiki_for_Collaborative_Documentation_TOSW/
layout: tagpage
tag: Using a Wiki for Collaborative Documentation TOSW
---

These are chapters of a small guide that says how to use a wiki for
collaborative documentation, the open source way.
