---
title: Contributing to TOSW intro
permalink: wiki/Contributing_to_TOSW_intro/
redirect_to: /wiki/How_to_contribute/
---

You should automatically be redirected to [How to contribute](/wiki/How_to_contribute/)
