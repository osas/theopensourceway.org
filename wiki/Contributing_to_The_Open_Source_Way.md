---
title: Contributing to The Open Source Way
permalink: wiki/Contributing_to_The_Open_Source_Way/
layout: wiki
tags:
 - The Open Source Way book
 - Contributing to TOSW
---

Edit the wiki
-------------

Fill in examples where it says *Example needed*.

Improve examples. We want examples that go beyond technology, especially
beyond free/libre open source software.

Point out when an entire new section is needed using the *discussion*
tab (aka the Talk: page.)

### Editing process

1.  Edit content as you see fit; **be bold**.
    -   That said, if you feel at all that you need support or
        permission, [Contacting the
        team](/wiki/Contacting_the_team "wikilink") is a good page to use. We
        want you to feel comfortable about what you edit. Don't worry,
        there is no hurry, we have time to get it right.
2.  Everytime you save the page, include a *Summary:* that explains why
    you changed the content; an overview of the changes is acceptable
    but not required (it's visible from the *history* tab.)
    -   Your first summary can include text that you are beginning one
        or more edits.
    -   You are encouraged to save often to prevent content loss in the
        case of browser crash or mistake.
        -   Alternately, take a look at the value of this Firefox
            plugin:
            <https://addons.mozilla.org/en-US/firefox/addon/4125>
3.  When you are done with your edit, have the last *Summary:* text
    include the words, "Ready for XML conversion."
    -   If you add content after that, repeat the usage of, "Ready for
        XML conversion" in each summary. This just moves forward the
        conversion point.
    -   This technique lets the XML conversion contributor know the
        boundary limit when doing a diff of the page history.
4.  When you are done with your edits and it is ready for XML
    conversion, send email to the [document lead or
    team](/wiki/User%3AQuaid "wikilink") that the changes are ready.

