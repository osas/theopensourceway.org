---
title: The Open Source Way:About
permalink: wiki/The_Open_Source_Way:About/
layout: wiki
---

This guide is for helping people to understand how to and how not to
engage with community over projects such as software, content,
marketing, art, infrastructure, standards, and so forth. It contains
knowledge distilled from years of Red Hat experience, which itself comes
from the many years of experience of individual upstream contributors
who have worked for Red Hat.

*[`Let`` ``your`` ``audience`` ``do`` ``your`` ``PR.`` ``They're`` ``pleasantly`` ``biased`` ``and`` ``decidedly`` ``not`` ``you.`](https://twitter.com/rands/statuses/2033668001)*

A bit of wisdom from [Rands](http://www.randsinrepose.com/) that applies
to communities and contributions - they are pleasantly biased and
decidedly not you.

Contributing to this content
----------------------------

A specific **goal** of this book is *to have a community write about
community*.

This wiki is the *upstream* for the book and is where all of the
collaboration on writing takes place. We can then form wiki-specific
processes over time within the authoring community. That process is
written up in [Contributing to The Open Source
Way](/wiki/Contributing_to_The_Open_Source_Way "wikilink").

A second step is the conversion of the content in to DocBook XML for
publication. That process is written up in [Converting to DocBook
XML](/wiki/Converting_to_DocBook_XML "wikilink").

The DocBook XML source is kept in a `git` repository at
<http://fedorahosted.org/tosw> . Details on getting accessing, using,
and contributing to that repository are in [Contributing to The Open
Source Way](/wiki/Contributing_to_The_Open_Source_Way "wikilink") and [Using
git repository](/wiki/Using_git_repository "wikilink").
